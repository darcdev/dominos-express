<?php

namespace Auth;

use Config\Database;
use PDO;

require_once 'libs/php-jwt/vendor/autoload.php';

include_once 'libs/php-jwt/vendor/firebase/php-jwt/src/BeforeValidException.php';
include_once 'libs/php-jwt/vendor/firebase/php-jwt/src/ExpiredException.php';
include_once 'libs/php-jwt/vendor/firebase/php-jwt/src/SignatureInvalidException.php';



use Firebase\JWT\JWT;

class Auth{
    private static $encrypt = array('HS256');
    #private static $aud = null;


    public static function generateJWT($data){

        $secret_key = parse_ini_file('config/secret_key.ini')['JWT_AUTH_SECRET_KEY'];
        $time = time();

        $token = array(
            'iat' => $time,
            'exp' => $time + (60*60*60),
            #'aud' => self::Aud(),
            'data' => $data
        );

        return JWT::encode($token, $secret_key);
    }

    public function validateJWT($jwt){
        $secret_key = parse_ini_file('config/secret_key.ini')['JWT_AUTH_SECRET_KEY'];
        
        if($jwt){

            try {
                $decoded = JWT::decode($jwt , $secret_key , self::$encrypt);
                $exp = $decoded->exp;
                
                if($decoded && ($exp > time())){
                    
                    $database = new Database();
                    $db = $database->getConnection();
                    
                    $correo = $this->getData($jwt)->correo;
                    
                    $query = "SELECT *
                    FROM vista_correos
                    WHERE correo = ?";
            
                    $stmt = $db->prepare($query);
                    $stmt->bindParam(1, $correo , PDO::PARAM_STR);
                    
                    $stmt->execute();
            
                    $num = $stmt->rowCount();
            
                    if($num > 0){
                        
                        return true;
                    }else{
                        return false;
                    }
                
                }
                
            } catch (\Exception $e) {
                
                return false;
            }

        }
        
        return false;
    }


    public static function getData($jwt){
        $secret_key = parse_ini_file('config/secret_key.ini')['JWT_AUTH_SECRET_KEY'];
        return JWT::decode(
            $jwt,
            $secret_key,
            self::$encrypt
        )->data;
    }


}
CREATE MATERIALIZED VIEW vista_correos AS 
SELECT correo FROM administradores
UNION all
SELECT correo FROM repartidores
UNION all
SELECT correo FROM tiendas
UNION all
SELECT correo FROM clientes
ORDER BY correo ASC;


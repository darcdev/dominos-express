CREATE MATERIALIZED VIEW vista_categorias AS
SELECT DISTINCT ct.cod_cat as cod_cat_tienda,ct.nombre_cat as tienda,cp.cod_categoria as cod_cat_producto,cp.nombre_cat as producto FROM categorias_tienda ct
JOIN tiendas t ON ct.cod_cat=t.cod_cat
JOIN provee pv ON pv.id_tienda=t.id
JOIN productos pr ON pv.cod_producto=pr.cod_producto
JOIN categorias_productos cp ON cp.cod_categoria=pr.cod_categoria
ORDER BY ct.cod_cat ASC;

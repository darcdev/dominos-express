CREATE TABLE clientes (
	id serial NOT NULL PRIMARY KEY,
	nombre character varying NOT NULL,
	apellido character varying NOT NULL,
	telefono int8 NOT NULL,
	direccion character varying NOT NULL,
	correo character varying NOT NULL UNIQUE,
	password character varying NOT NULL,
	estado boolean DEFAULT true
);


CREATE TABLE tiendas (
	id serial NOT NULL PRIMARY KEY,
	nombre character varying NOT NULL,
	logo character varying NOT NULL,	
	direccion character varying NOT NULL,		
	cod_cat integer ,
	id_rol integer ,
	correo character varying NOT NULL UNIQUE,
	password character varying NOT NULL,
	estado boolean DEFAULT true
);
ALTER TABLE tiendas ADD CONSTRAINT pertenence FOREIGN KEY (cod_cat) REFERENCES categorias_tienda(cod_cat);
ALTER TABLE tiendas ADD CONSTRAINT asocia1 FOREIGN KEY (id_rol) REFERENCES roles(id_rol);


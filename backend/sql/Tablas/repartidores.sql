CREATE TABLE repartidores (
	id serial NOT NULL PRIMARY KEY,
	nombre character varying NOT NULL ,
	apellido character varying NOT NULL,
	estado boolean NOT NULL DEFAULT true,
	id_rol integer,
	correo character varying NOT NULL UNIQUE,
	password character varying NOT NULL
);
ALTER TABLE repartidores ADD CONSTRAINT asocia1 FOREIGN KEY (id_rol) REFERENCES roles(id_rol);


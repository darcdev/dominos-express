CREATE TABLE pedidos (
	cod_pedido serial NOT NULL PRIMARY KEY,
	estado boolean NOT NULL DEFAULT false,
	fecha_pedido date NOT NULL,
	hora_pedido TIME NOT NULL,
	hora_entrega TIME,
	id_cliente integer ,
	id_repartidor integer
);

ALTER TABLE pedidos ADD CONSTRAINT domicilia FOREIGN KEY (id_repartidor) REFERENCES repartidores(id);
ALTER TABLE pedidos ADD CONSTRAINT pide FOREIGN KEY (id_cliente) REFERENCES clientes(id);



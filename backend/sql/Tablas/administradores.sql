CREATE TABLE administradores (
	id serial,
	nombre character varying NOT NULL,
	apellido character varying,
	id_rol integer ,
	correo character varying NOT NULL UNIQUE,
	password character varying NOT NULL
);
ALTER TABLE administradores ADD CONSTRAINT asocia2 FOREIGN KEY (id_rol) REFERENCES roles(id_rol);

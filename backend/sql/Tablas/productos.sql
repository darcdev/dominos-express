CREATE TABLE productos (
	cod_producto serial NOT NULL PRIMARY KEY,
	nombre_producto character varying NOT NULL UNIQUE,
	precio integer NOT NULL,
	poster character varying,
	stock integer NOT NULL,
	descripcion character varying,
	cod_categoria integer ,
	estado boolean DEFAULT true
);
ALTER TABLE productos ADD CONSTRAINT pertenece FOREIGN KEY (cod_categoria) REFERENCES categorias_productos(cod_categoria);

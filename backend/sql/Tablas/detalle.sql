CREATE TABLE detalle (
	cod_producto integer,
	cod_pedido integer,
	cantidad integer NOT NULL,
	comentario character varying,
	total integer
);
ALTER TABLE detalle ADD CONSTRAINT referencia FOREIGN KEY (cod_producto) REFERENCES productos(cod_producto);
ALTER TABLE detalle ADD CONSTRAINT solicitud FOREIGN KEY (cod_pedido) REFERENCES pedidos(cod_pedido);

<?php

namespace Config;

use PDO;

class Database{
    
    private $conn;


    /**
     * Get the value of conn
     */ 
    public function getConn()
    {
        return $this->conn;
    }

    /**
     * Set the value of conn
     *
     * @return  self
     */ 
    public function setConn($conn)
    {
        $this->conn = $conn;

        return $this;
    }

 
    public function getConnection(){
 
        $params = parse_ini_file('database.ini');
        
        try{

            $conndb = new PDO("pgsql:host=" . $params['host'] . ";dbname=" . $params['db_name'], $params['username'], $params['password']);
            $this->setConn($conndb);

        }catch(PDOException $exception){
            echo "Connection error: " . $exception->getMessage();
        }
 
        return $this->getConn();
    }

}
?>


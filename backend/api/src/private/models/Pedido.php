<?php

namespace Models;
use Config\Database;
use PDO;


class Pedido {

    public $table_name = "pedidos";
    
    private $cod_pedido;
    private $estado;
    private $hora_entrega;
    private $hora_pedido;
    private $fecha_pedido;
    private $id_cliente;
    private $id_repartidor;
    private $id_tienda;


    public function insertar($id_cliente , $id_repartidor){
        
        $database = new Database();
        $db = $database->getConnection();

        $query = "INSERT INTO pedidos(id_cliente,id_repartidor,fecha_pedido,hora_pedido)
                  VALUES(?,?, (SELECT current_date) ,(SELECT current_time) )";

        $stmt = $db->prepare($query);

        $stmt->bindParam(1 , $id_cliente , PDO::PARAM_INT);
        $stmt->bindParam(2 , $id_repartidor , PDO::PARAM_INT);

        if($stmt->execute()){
            http_response_code(200);
        }else{
            http_response_code(401);
        }

    }


    public function codigoActual(){

        $database = new Database();
        $db = $database->getConnection();

        $query = "SELECT currval('pedidos_cod_pedido_seq')";

        $stmt = $db->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        return $result;

    }

    /**
     * Get the value of cod_pedido
     */ 
    public function getCod_pedido()
    {
        return $this->cod_pedido;
    }

    /**
     * Set the value of cod_pedido
     *
     * @return  self
     */ 
    public function setCod_pedido($cod_pedido)
    {
        $this->cod_pedido = $cod_pedido;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of hora_entrega
     */ 
    public function getHora_entrega()
    {
        return $this->hora_entrega;
    }

    /**
     * Set the value of hora_entrega
     *
     * @return  self
     */ 
    public function setHora_entrega($hora_entrega)
    {
        $this->hora_entrega = $hora_entrega;

        return $this;
    }

    /**
     * Get the value of hora_pedido
     */ 
    public function getHora_pedido()
    {
        return $this->hora_pedido;
    }

    /**
     * Set the value of hora_pedido
     *
     * @return  self
     */ 
    public function setHora_pedido($hora_pedido)
    {
        $this->hora_pedido = $hora_pedido;

        return $this;
    }

    /**
     * Get the value of fecha_pedido
     */ 
    public function getFecha_pedido()
    {
        return $this->fecha_pedido;
    }

    /**
     * Set the value of fecha_pedido
     *
     * @return  self
     */ 
    public function setFecha_pedido($fecha_pedido)
    {
        $this->fecha_pedido = $fecha_pedido;

        return $this;
    }
}


?>

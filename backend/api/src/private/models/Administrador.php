<?php

namespace Models;

class Administrador extends BaseUser {

    public $table_name = "administradores";

    private $cod_admin;
    private $apellido;
    private $id_rol;




    /**
     * Get the value of cod_admin
     */ 
    public function getCod_admin()
    {
        return $this->cod_admin;
    }

    /**
     * Set the value of cod_admin
     *
     * @return  self
     */ 
    public function setCod_admin($cod_admin)
    {
        $this->cod_admin = $cod_admin;

        return $this;
    }

    

    /**
     * Get the value of apellido
     */ 
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set the value of apellido
     *
     * @return  self
     */ 
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get the value of id_rol
     */ 
    public function getId_rol()
    {
        return $this->id_rol;
    }

    /**
     * Set the value of id_rol
     *
     * @return  self
     */ 
    public function setId_rol($id_rol)
    {
        $this->id_rol = $id_rol;

        return $this;
    }
}


?>
<?php

namespace Models;

use Config\Database;
use PDO;


class BaseUser {
    
    private $correo;
    private $nombre;
    private $password;
    private $estado;

    public function emailExists($correo){

        $database = new Database();
        $db = $database->getConnection();

        $query = "SELECT *
        FROM vista_correos
        WHERE correo = ?
        LIMIT 1";

        $stmt = $db->prepare($query);
        $stmt->bindParam(1, $correo , PDO::PARAM_STR);
        
        $stmt->execute();
        $num = $stmt->rowCount();
        
        if($num > 0){
            
            return true;
        }
    
        return false;

    }


    public function getDataSettedEmail(){

        $database = new Database();
        $db = $database->getConnection();

        $query = "SELECT *
                  FROM $this->table_name
                  WHERE correo = ?
                  LIMIT 1";

        $stmt = $db->prepare($query);

        $correo = $this->getCorreo();

        $stmt->bindParam(1 , $correo , PDO::PARAM_STR);

        $stmt->execute();
        $num = $stmt->rowCount();

        if($num > 0){
            $user = $stmt->fetch(PDO::FETCH_ASSOC);

            return $user;
        }

        return NULL;

    }


    /**
     * Get the value of correo
     */ 
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * Set the value of correo
     *
     * @return  self
     */ 
    public function setCorreo($correo)
    {
        $this->correo = $correo;

        return $this;
    }

    /**
     * Get the value of nombre
     */ 
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set the value of nombre
     *
     * @return  self
     */ 
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * Get the value of password
     */ 
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set the value of password
     *
     * @return  self
     */ 
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }
}


?>
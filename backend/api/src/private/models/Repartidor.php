<?php

namespace Models;
use Config\Database;
use PDO;


class Repartidor extends BaseUser {

    public $table_name = "repartidores";

    private $cod_repartidor;
    private $apellido;
    private $estado;
    private $id_rol;


    public function totalRepartidores(){

        $database = new Database();
        $db = $database->getConnection();

        $query = "SELECT COUNT(*) AS total
                  FROM repartidores";

        $stmt = $db->prepare($query);

        $stmt->execute();
        $total = $stmt->fetch(PDO::FETCH_ASSOC);

        return $total;

    }


    /**
     * Get the value of cod_repartidor
     */ 
    public function getCod_repartidor()
    {
        return $this->cod_repartidor;
    }

    /**
     * Set the value of cod_repartidor
     *
     * @return  self
     */ 
    public function setCod_repartidor($cod_repartidor)
    {
        $this->cod_repartidor = $cod_repartidor;

        return $this;
    }

    /**
     * Get the value of apellido
     */ 
    public function getApellido()
    {
        return $this->apellido;
    }

    /**
     * Set the value of apellido
     *
     * @return  self
     */ 
    public function setApellido($apellido)
    {
        $this->apellido = $apellido;

        return $this;
    }

    /**
     * Get the value of estado
     */ 
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set the value of estado
     *
     * @return  self
     */ 
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get the value of id_rol
     */ 
    public function getId_rol()
    {
        return $this->id_rol;
    }

    /**
     * Set the value of id_rol
     *
     * @return  self
     */ 
    public function setId_rol($id_rol)
    {
        $this->id_rol = $id_rol;

        return $this;
    }
}


?>
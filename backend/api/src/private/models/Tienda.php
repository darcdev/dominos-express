<?php

namespace Models;
use Config\Database;
use PDO;

class Tienda extends BaseUser {

    public $table_name = "tiendas";

    private $cod_tienda;
    private $direccion;
    private $cod_cat;
    private $logo;  /* PENDIENTE REVISAR IMAGE STORAGE */
    private $id_rol;
    

    public function registrarProductoPedido($id_tienda , $cod_pedido){

        $database = new Database();
        $db = $database->getConnection();

        $query = "INSERT INTO registra(id_tienda,cod_pedido)
                  VALUES(?,?)";

        $stmt = $db->prepare($query);

        $stmt->bindParam(1 , $id_tienda , PDO::PARAM_INT);
        $stmt->bindParam(2 , $cod_pedido , PDO::PARAM_INT);

        if($stmt->execute()){
            http_response_code(200);
        }else{
            http_response_code(401);
        }


    }

    public function nameMarketExists($nombre_tienda){

        $database = new Database();
        $db = $database->getConnection();

        $query = "SELECT *
        FROM tiendas
        WHERE nombre = ?
        LIMIT 1";

        $stmt = $db->prepare($query);
        $stmt->bindParam(1, $nombre_tienda , PDO::PARAM_STR);
        
        $stmt->execute();

        $num = $stmt->rowCount();

        if($num > 0){
            
            return true;
        }
    
        return false;

    }


    public function categoryExists($nombre_cat){

        $database = new Database();
        $db = $database->getConnection();

        $query = "SELECT *
        FROM categorias_tienda
        WHERE nombre_cat = ?
        LIMIT 1";

        $stmt = $db->prepare($query);
        $stmt->bindParam(1, $nombre_cat , PDO::PARAM_STR);
        
        $stmt->execute();

        $num = $stmt->rowCount();

        if($num > 0){
            
            return true;
        }
    
        return false;
        
    }

    
    public function addCategory($nombre_cat){
        $database = new Database();
        $db = $database->getConnection();

        $query = "INSERT INTO categorias_tienda(nombre_cat) VALUES(?)";

        $stmt = $db->prepare($query);
        $stmt->bindParam(1, $nombre_cat , PDO::PARAM_STR);
        
        $stmt->execute();
    }
    


    /**
     * Get the value of cod_tienda
     */ 
    public function getCod_tienda()
    {
        return $this->cod_tienda;
    }

    /**
     * Set the value of cod_tienda
     *
     * @return  self
     */ 
    public function setCod_tienda($cod_tienda)
    {
        $this->cod_tienda = $cod_tienda;

        return $this;
    }

    /**
     * Get the value of direccion
     */ 
    public function getDireccion()
    {
        return $this->direccion;
    }

    /**
     * Set the value of direccion
     *
     * @return  self
     */ 
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;

        return $this;
    }

    /**
     * Get the value of cod_cat
     */ 
    public function getCod_cat()
    {
        return $this->cod_cat;
    }

    /**
     * Set the value of cod_cat
     *
     * @return  self
     */ 
    public function setCod_cat($cod_cat)
    {
        $this->cod_cat = $cod_cat;

        return $this;
    }

    /**
     * Get the value of logo
     */ 
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set the value of logo
     *
     * @return  self
     */ 
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get the value of id_rol
     */ 
    public function getId_rol()
    {
        return $this->id_rol;
    }

    /**
     * Set the value of id_rol
     *
     * @return  self
     */ 
    public function setId_rol($id_rol)
    {
        $this->id_rol = $id_rol;

        return $this;
    }
}


?>
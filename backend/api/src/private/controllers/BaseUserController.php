<?php

namespace Controllers;

use Config\Database;
use Auth\Auth;
use PDO;


class BaseUserController {


    public function login($request){
        
        if($request->getMethod() == 'POST'){
            
            $data = json_decode(file_get_contents("php://input"));

            $correo = $request->getServerParams()["PHP_AUTH_USER"];
            $password = $request->getServerParams()["PHP_AUTH_PW"];
            
            $user = new $this->model_name;
            $email_exists= $user->emailExists($correo);
            
            if($email_exists){

                $user->setCorreo($correo);
                $data_user = $user->getDataSettedEmail();

                $status = $data_user['estado'];
                
                if($status == true){
                    
                    $data_user['tipo_usuario'] = $data->tipo_usuario;
                    
                    #if(password_verify($data->password , $data_user['password'])){
                    if($password == $data_user['password']){
    
                        $auth = new Auth();
    
                        $data_in_token = array(
                            "id" => $data_user['id'],
                            "tipo_usuario" => $data->tipo_usuario,
                            "correo" => $correo
                        );
    
                        if(!isset($data_user['apellido'])){
                            $user_object = (object) array(
                                "id" => $data_user['id'],
                                "correo" => $correo,
                                "rol" => $data->tipo_usuario,
                                "nombre" => $data_user['nombre']
                            );
                        }else{
                            $user_object = (object) array(
                                "id" => $data_user['id'],
                                "correo" => $correo,
                                "rol" => $data->tipo_usuario,
                                "nombre" => $data_user['nombre'],
                                "apellido" => $data_user['apellido']
                            );
                        }
    
    
                        $jwt = $auth->generateJWT($data_in_token);
                        
                         // set response code
                         http_response_code(200);
    
                         echo json_encode(
                                 array(
                                     "message" => "Successful login.",
                                     "jwt" => $jwt,
                                     "user" => $user_object
                                 )
                             );
    
                             
                    }else{
     
                        // set response code
                        http_response_code(401);
                     
                        // tell the user login failed
                        echo json_encode(array("message" => "Login failed."));
                    }
                
                }else{
                    http_response_code(401);
                    echo json_encode(array("message" => "Inactive account."));
                }

            }else{
 
                // set response code
                http_response_code(401);
             
                // tell the user login failed
                echo json_encode(array("message" => "Login failed."));
            }

        }
        

    }

    public function getToken($request){

        $authorization_token = $request->getHeaders()['authorization'][0];
            
        $token = explode(' ' , $authorization_token)[1];
        
        return $token;

    }


    public function getRol($token){
        $auth = new Auth();
        $data_in_token = $auth->getData($token);
        $rol = $data_in_token->tipo_usuario;
        
        return $rol; 

    }


    public function inactiveUser($request){

        if($request->getMethod() == 'PATCH'){

            $token = $this->getToken($request);
            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            var_dump($verified_token);
            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'repartidores' || $rol == 'clientes' || $rol == 'tiendas'){

                    $tipo_usuario = $request->getAttribute('user');
                    $id = $request->getAttribute('id');

                    $estado = false;
    
                    $query = "UPDATE $tipo_usuario SET estado = ?
                                WHERE id = ?";
    
                    $database = new Database();
                    $db = $database->getConnection();
    
                    $stmt = $db->prepare($query);
                    
                    $stmt->bindParam(1 , $estado , PDO::PARAM_BOOL);
                    $stmt->bindParam(2 , $id , PDO::PARAM_INT);
    
                    $stmt->execute();

                }else{
                    http_response_code(401);             
                    echo json_encode(array("message" => "Access denied."));     
                }
                

            }else{ 
                http_response_code(401);             
                echo json_encode(array("message" => "Access denied."));
            }
            
        }

    }


    public function personalInfo($request){

        if($request->getMethod() == 'GET'){

            $auth = new Auth();            
            $token = $this->getToken($request);
            
            $verified_token = $auth->validateJWT($token);

            if($verified_token){
                
                $data_token = $auth->getData($token);
                $user = new $this->model_name;
                $user->setCorreo($data_token->correo);
                $data_user = (object) $user->getDataSettedEmail();
                
                $data_to_send = array(
                    'user' => $data_user
                );

                http_response_code(200);
                
                echo json_encode($data_to_send);

            }else{
                http_response_code(401);             
                echo json_encode(array("message" => "Access denied."));
            }

        }

    }

    public function getProduct($request){

        if($request->getMethod() == 'GET'){
            
            $id = $request->getAttribute('id');
            $database = new Database();
            $db = $database->getConnection();
    
            $query = "SELECT p.cod_producto , p.stock , p.poster , p.descripcion , p.nombre_producto , p.precio , cp.nombre_cat
                        FROM productos AS p
                        JOIN categorias_productos AS cp ON cp.cod_categoria = p.cod_categoria
                        WHERE p.cod_producto = ?";
    
            $stmt = $db->prepare($query);
                
            $stmt->bindParam(1 , $id , PDO::PARAM_STR);
    
            if($stmt->execute()){
                $producto = (object) $stmt->fetch(PDO::FETCH_ASSOC);
                http_response_code(200);
                
                echo json_encode( 
                    array(
                    "producto" => $producto
                    )
                );
            
            }else{
                http_response_code(400);
                echo "Product does not exist";
            }

        }

    }


    public function obtenerPedidosPorEstado($request){

        if($request->getMethod() == 'GET'){

            
            $token = $this->getToken($request);
            

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $tipo_usuario = $request->getAttribute('user');

                if($tipo_usuario == 'administradores' || $tipo_usuario == 'tiendas'){

                    $estado = $request->getAttribute('estado');
    
                    $database = new Database();
                    $db = $database->getConnection();        
                    
                    if($estado == 'activos'){
    
                        $query = "SELECT p.cod_pedido, p.estado, p.hora_entrega, p.hora_pedido, p.fecha_pedido,
                                         c.id id_cliente, c.nombre nombre_cliente, c.apellido apellido_cliente, c.direccion,
                                         r.id id_repartidor, r.nombre nombre_repartidor, r.apellido apellido_repartidor,
                                         t.id id_tienda, t.nombre nombre_tienda
                                  FROM pedidos AS p
                                    JOIN clientes AS c ON c.id = p.id_cliente
                                    JOIN repartidores AS r ON r.id = p.id_repartidor
                                    JOIN registra AS reg ON reg.cod_pedido = p.cod_pedido
                                    JOIN tiendas AS t ON t.id = reg.id_tienda
                                  WHERE p.estado = true
                                  ORDER BY p.fecha_pedido, p.hora_pedido DESC";
    
                    }elseif($estado == 'inactivos'){
    
                        $query = "SELECT p.cod_pedido, p.estado, p.hora_entrega, p.hora_pedido, p.fecha_pedido,
                                         c.id id_cliente, c.nombre nombre_cliente, c.apellido apellido_cliente, c.direccion,
                                         r.id id_repartidor, r.nombre nombre_repartidor, r.apellido apellido_repartidor,
                                         t.id id_tienda, t.nombre nombre_tienda
                                  FROM pedidos AS p
                                    JOIN clientes AS c ON c.id = p.id_cliente
                                    JOIN repartidores AS r ON r.id = p.id_repartidor
                                    JOIN registra AS reg ON reg.cod_pedido = p.cod_pedido
                                    JOIN tiendas AS t ON t.id = reg.id_tienda
                                  WHERE p.estado = false
                                  ORDER BY p.fecha_pedido, p.hora_pedido DESC";

                    }else{
                        http_response_code(400);
                        echo json_encode(array("message" => "Resource does not exist."));
                    }
    
                    $stmt = $db->prepare($query);
                    $stmt->execute();
    
                    $num = $stmt->rowCount();

                    if($num > 0){
                        $pedidos = $stmt->fetchAll(PDO::FETCH_OBJ);
                        echo json_encode(
                            array(
                            "pedidos" => $pedidos
                            )
                        );

                    }

                }

            }else{ 
                http_response_code(401);             
                echo json_encode(array("message" => "Access denied."));
            }
            
        }

    }


    public function tiendasPorCategoria($request){

        if($request->getMethod() == 'GET'){

            
            #$token = $this->getToken($request);
            

            #$auth = new Auth();
            #$verified_token = $auth->validateJWT($token);
            
            #if($verified_token){

                #$rol = $this->getRol($token);

                #if($rol == 'administradores' || $rol == 'clientes'){

            $nombre_cat = $request->getAttribute('nombre_cat');

            $database = new Database();
            $db = $database->getConnection();
            
            $query = "SELECT t.id , t.nombre , t.logo
                        FROM tiendas AS t
                        JOIN categorias_tienda AS ct ON ct.cod_cat = t.cod_cat
                        WHERE ct.nombre_cat = ?";

            $stmt = $db->prepare($query);
            $stmt->bindParam(1 , $nombre_cat , PDO::PARAM_STR);
            $stmt->execute();
            $num = $stmt->rowCount();
            #echo $num;
            if($num > 0){
                $tiendas = $stmt->fetchAll(PDO::FETCH_OBJ);
                
                echo json_encode(
                    array(
                        "tiendas" => $tiendas
                    )
                );
                
            }else{
                echo json_encode(
                    array(
                        "tiendas" => array()
                    )
                );
            }

                #}else{
                    #http_response_code(401);             
                    #echo json_encode(array("message" => "Access denied."));
                #}
                


                /*             }else{ 
                                http_response_code(401);             
                                echo json_encode(array("message" => "Access denied."));
                            } */
            
        }

    }

    
    public function obtenerCategoriaTienda($request){

        if($request->getMethod() == 'GET'){
            
            $nombre_tienda = $request->getAttribute('nombre_tienda');
            $database = new Database();
            $db = $database->getConnection();
    
            $query = "SELECT ct.nombre_cat
                        FROM categorias_tienda AS ct
                        JOIN tiendas AS t ON t.cod_cat = ct.cod_cat
                        WHERE t.nombre = ?";
    
            $stmt = $db->prepare($query);
                
            $stmt->bindParam(1 , $nombre_tienda , PDO::PARAM_STR);
    
            if($stmt->execute()){
                $categoria = $stmt->fetch(PDO::FETCH_OBJ);
                http_response_code(200);
                
                echo json_encode( 
                    array(
                    "categoria" => $categoria
                    )
                );
            
            }else{
                echo json_encode( 
                    array(
                    "categoria" => ""
                    )
                );
            }

        }
        

    }


    public function obtenerProductosPorCategoriaTienda($request){

        if($request->getMethod() == 'GET'){

                    
            $nombre_tienda = $request->getAttribute('nombre_tienda');
            $categoria_producto = $request->getAttribute('categoria_producto');

            $database = new Database();
            $db = $database->getConnection();        
            
            $query = "SELECT p.poster, p.cod_producto, p.nombre_producto, p.precio , p.stock, p.descripcion
                        FROM productos AS p
                        JOIN provee AS prov ON prov.cod_producto = p.cod_producto
                        JOIN tiendas AS t ON t.id = prov.id_tienda
                        JOIN categorias_productos AS cp ON cp.cod_categoria = p.cod_categoria
                        WHERE t.nombre = ? AND cp.nombre_cat = ?";
            

            $stmt = $db->prepare($query);
            $stmt->bindParam(1 , $nombre_tienda , PDO::PARAM_STR);
            $stmt->bindParam(2 , $categoria_producto , PDO::PARAM_STR);
            $stmt->execute();

            $num = $stmt->rowCount();
            #echo $num;
            if($num > 0){
                $productos = $stmt->fetchAll(PDO::FETCH_OBJ);

                echo json_encode(
                    array(
                        "message" => "Success query",
                        "productos" => $productos
                    )
                );

            }else{
                echo json_encode(array("message" => "The market does not match products."));    
            }

            
        }

    }




}
<?php

namespace Controllers;

use Config\Database;
use Models\{Cliente,Pedido,Tienda,Repartidor};
use Auth\Auth;
use PDO;


class ClientController extends BaseUserController {

    public $model_name = "Models\Cliente";

    public function createClient($request){
        $data = json_decode(file_get_contents("php://input"));
        $client = new Cliente();
        $email_exists = $client->emailExists($data->correo);
        
        if($request->getMethod() == 'POST'){

            if(!empty($data->nombre) &&
               !empty($data->password) &&
               !empty($data->correo)){

                if(!$email_exists){

                    $database = new Database();
                    $db = $database->getConnection();
        
                    $client->setNombre($data->nombre);
                    $client->setApellido($data->apellido);
                    $client->setCorreo($data->correo);
                    $client->setPassword($data->password);
                    $client->setTelefono($data->telefono);
                    $client->setDireccion($data->direccion);
        
                    $query = "INSERT INTO " . $client->table_name . "(nombre , apellido , correo , password , telefono , direccion) VALUES(? , ? , ? , ? , ? , ?)";
                    $stmt = $db->prepare($query);
                    $stmt->bindParam(1, $data->nombre , PDO::PARAM_STR);
                    $stmt->bindParam(2, $data->apellido , PDO::PARAM_STR);
                    $stmt->bindParam(3, $data->correo , PDO::PARAM_STR);
                
            
                    $password_hash = password_hash($data->password, PASSWORD_BCRYPT);
                    $stmt->bindParam(4, $password_hash , PDO::PARAM_STR);
                    $stmt->bindParam(5, $data->telefono , PDO::PARAM_STR);
                    $stmt->bindParam(6, $data->direccion , PDO::PARAM_STR);
    
                    $stmt->execute();        
                    
                    echo json_encode(array("message" => "client was created."));
                    http_response_code(200);
                
                }else{
                    echo json_encode(array("message" => "Email already exists"));
                }

             
            }
             
            else{
                
                echo json_encode(array("message" => "Unable to create client."));
                http_response_code(400);
             
            }
            

        }
    }

    
    public function updateClient($request){

        if($request->getMethod() == 'PATCH'){

            $token = $this->getToken($request);

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $rol = $this->getRol($token);
                
                if($rol == 'clientes'){
                    
                    $data = json_decode(file_get_contents("php://input"));
                    $client = new Cliente();
                    $email_exists = $client->emailExists($data->correo);

                    if(!$email_exists){
                        
                        $id = $auth->getData($token)->id;
                        $database = new Database();
                        $db = $database->getConnection();
                
                        $query = "UPDATE clientes
                                    SET nombre = ? , apellido = ? , direccion = ? , correo = ? , telefono = ? , password = ?
                                    WHERE id = ? ";
                
                        $stmt = $db->prepare($query);
                        
                        $password_hash = password_hash($data->password, PASSWORD_BCRYPT);
                        $stmt->bindParam(1 , $data->nombre , PDO::PARAM_STR);
                        $stmt->bindParam(2 , $data->apellido , PDO::PARAM_STR);
                        $stmt->bindParam(3 , $data->direccion , PDO::PARAM_STR);
                        $stmt->bindParam(4 , $data->correo , PDO::PARAM_STR);
                        $stmt->bindParam(5 , $data->telefono , PDO::PARAM_STR);
                        $stmt->bindParam(6 , $password_hash , PDO::PARAM_STR);
                        $stmt->bindParam(7 , $id , PDO::PARAM_INT);
                        
                        if($stmt->execute()){
                            http_response_code(200);
                            echo json_encode(
                                array(
                                    "message" => "successful query"
                                )
                            );
    
                        }else{
                            http_response_code(400);
                            echo json_encode(
                                array(
                                    "message" => "no successful query"
                                )
                            );
                        }
                        
                    }else{
                        http_response_code(406);
                        echo json_encode(
                            array(
                                "message" => "Email already exists"
                            )
                        );
                    }


                }else{
                    http_response_code(401);
                    echo json_encode(
                        array(
                            "message" => "Denied Access"
                        )
                    );
                }
                
            }else{
                http_response_code(401);
                echo json_encode(
                    array(
                        "message" => "Denied Access"
                    )
                );
            }

        }

    }


    public function ordenarPedido($request){

        if($request->getMethod() == 'POST'){

            $token = $this->getToken($request);

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'clientes'){

                    $data = json_decode(file_get_contents("php://input"));

                    $tienda = new Tienda();
                    $repartidores = new Repartidor();
                    $pedido = new Pedido();
                    $database = new Database();
                    $db = $database->getConnection();
                    $pedido_info = $data->pedido;

                    $id_cliente = $data->id_cliente;
                    $total_repartidores = $repartidores->totalRepartidores()['total'];

                    $id_repartidor = mt_rand(1,$total_repartidores);
                    $cod_pedido = $pedido->codigoActual()['currval'] + 1;
             
                    foreach ($pedido_info as $key => $value) {
                        $id_tienda = $value->id_tienda;
                        $cod_producto = $value->cod_producto;
                        $total = $value->total;
                        $cantidad = $value->cantidad;
                        $comentario = $value->comentario;
                        $tienda->registrarProductoPedido($id_tienda,$cod_pedido);
                        $pedido->insertar($id_cliente , $id_repartidor);

                        $query = "INSERT INTO detalle(cod_pedido,cod_producto,total,cantidad,comentario)
                                  VALUES(?,?,?,?,?)";
                        
                        $stmt = $db->prepare($query);

                        $stmt->bindParam(1,$cod_pedido,PDO::PARAM_INT);
                        $stmt->bindParam(2,$cod_producto,PDO::PARAM_INT);
                        $stmt->bindParam(3,$total,PDO::PARAM_INT);
                        $stmt->bindParam(4,$cantidad,PDO::PARAM_INT);
                        $stmt->bindParam(5,$comentario,PDO::PARAM_STR);

                        if($stmt->execute()){
                            http_response_code(200);
                        }else{
                            http_response_code(406);
                        }

                    }

                    return json_encode(
                        array(
                            "message" => "successful query"
                        )
                    );



                }else{
                    http_response_code(401);
                    return json_encode(
                        array(
                            "message" => "Denied Access"
                        )
                    );
                }
                
            }else{
                http_response_code(401);
                return json_encode(
                    array(
                        "message" => "Denied Access"
                    )
                );
            }

        }

    }
    

    public function categoriasProductosPorTienda($request){

        if($request->getMethod() == 'GET'){

            /*             $token = $this->getToken($request);

                        $auth = new Auth();
                        $verified_token = $auth->validateJWT($token); */
            
            /*             if($verified_token){

                            $rol = $this->getRol($token);

                            if($rol == 'clientes'){ */

            $nombre_tienda = $request->getAttribute('nombre_tienda');

            $database = new Database();
            $db = $database->getConnection();

            $query = "SELECT DISTINCT cp.nombre_cat
                        FROM categorias_productos AS cp
                        JOIN productos AS p ON p.cod_categoria = cp.cod_categoria
                        JOIN provee AS prov ON prov.cod_producto = p.cod_producto
                        JOIN tiendas AS t ON t.id = prov.id_tienda 
                        WHERE t.nombre = ?";
            
            $stmt = $db->prepare($query);

            $stmt->bindParam(1,$nombre_tienda,PDO::PARAM_STR);

            $stmt->execute();

            $num = $stmt->rowCount();

            if($num > 0){

                $categorias = $stmt->fetchAll(PDO::FETCH_OBJ);

                echo json_encode(
                    array(
                        "message" => "successful query",
                        "categorias" => $categorias
                    )
                );

                }else{
                    echo json_encode(
                        array(
                            "message" => "NO successful query"
                        )
                    );   
                }




        }else{
            http_response_code(401);
            echo json_encode(
                array(
                    "message" => "Denied Access"
                )
            );   
        }
                
                    /*             }else{
                                http_response_code(401);
                                echo json_encode(
                                    array(
                                        "message" => "NO successful query"
                                    )
                                );   
                            } */   

    }

    


}
?>
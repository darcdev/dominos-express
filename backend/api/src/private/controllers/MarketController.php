<?php

namespace Controllers;

use Config\Database;
use Models\{Tienda , Producto};
use Auth\Auth;
use PDO;


class MarketController extends BaseUserController {

    public $model_name = "Models\Tienda";

    public function createMarket($request){

        if($request->getMethod() == 'POST'){
            $data = json_decode(file_get_contents("php://input"));
            $market = new Tienda();
            $email_exists = $market->emailExists($data->correo);

            $name_market_exists = $market->nameMarketExists($data->nombre);

            
                if(!$email_exists && !$name_market_exists){

                    $database = new Database();
                    $db = $database->getConnection();

                    
                    $query = "INSERT INTO " . $market->table_name . "(nombre , logo , correo , password , direccion,cod_cat, id_rol) VALUES(? , ? , ? , ? , ? , ? , ?)";
                    $stmt = $db->prepare($query);
                    $stmt->bindParam(1, $data->nombre , PDO::PARAM_STR);
                    $stmt->bindParam(2, $data->logo , PDO::PARAM_STR);
                    $stmt->bindParam(3, $data->correo , PDO::PARAM_STR);
                    
                    
                    $id_rol = 2;
                    $password_hash = password_hash($data->password, PASSWORD_BCRYPT);
                    $stmt->bindParam(4, $password_hash , PDO::PARAM_STR);
                    $stmt->bindParam(5, $data->direccion , PDO::PARAM_STR);
                    $stmt->bindParam(6, $data->cod_cat , PDO::PARAM_INT);
                    $stmt->bindParam(7, $id_rol , PDO::PARAM_INT);
                    var_dump($stmt->execute());        
                    
                    echo json_encode(array("message" => "market was created."));
                    http_response_code(200);
                
                }elseif($email_exists && $name_market_exists){
                    echo json_encode(array("message" => "Email and name Market already exist"));
                }elseif($email_exists && !$name_market_exists){
                    echo json_encode(array("message" => "Email already exists"));
                }elseif(!$email_exists && $name_market_exists){
                    echo json_encode(array("message" => "Name Market already exists"));
                }
            

        }
   
    }

    public function updateMarket($request){

        if($request->getMethod() == 'PATCH'){

            $token = $this->getToken($request);

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'tiendas'){
                    
                    $id = $auth->getData($token)->id;
                    $data = json_decode(file_get_contents("php://input"));
                    $market = new Tienda();

                    $email_exists = $market->emailExists($data->correo);

                    if(!$email_exists){

                        $database = new Database();
                        $db = $database->getConnection();
                
                        $query = "UPDATE tiendas
                                    SET logo = ? , nombre = ? , correo = ? , password = ? , direccion = ?
                                    WHERE id = ? ";
                
                        $stmt = $db->prepare($query);
                            
                        $stmt->bindParam(1 , $data->logo , PDO::PARAM_STR);
                        $stmt->bindParam(2 , $data->nombre , PDO::PARAM_STR);
                        $stmt->bindParam(3 , $data->correo , PDO::PARAM_STR);
                        $stmt->bindParam(4 , $data->password , PDO::PARAM_STR);
                        $stmt->bindParam(5 , $data->direccion , PDO::PARAM_STR);
                        $stmt->bindParam(6 , $id , PDO::PARAM_INT);
                        
                        if($stmt->execute()){
                            http_response_code(200);
                            echo json_encode(
                                array(
                                    "message" => "successful query"
                                )
                            );
    
                        }else{
                            http_response_code(400);
                            echo json_encode(
                                array(
                                    "message" => "no successful query"
                                )
                            );
                        }

                    }else{
                        echo json_encode(
                            array(
                                "message" => "Email already exists"
                            )
                        );
                        }
                    

                }else{
                    http_response_code(401);
                    echo json_encode(
                        array(
                            "message" => "Denied Access"
                        )
                    );
                }
                
            }else{
                http_response_code(401);
                echo json_encode(
                    array(
                        "message" => "Denied Access"
                    )
                );
            }

        }

    }


    public function agregarProducto($request){

        if($request->getMethod() == 'POST'){

            $market = new Tienda();
            $auth = new Auth();
            $token = $this->getToken($request);

            $verified_token = $auth->validateJWT($token);

            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'tiendas'){
                    
                    $data = json_decode(file_get_contents("php://input"));
    
                    $database = new Database();
                    $db = $database->getConnection();
    
                    $query = "INSERT INTO productos(nombre_producto,precio,poster,stock,descripcion,cod_categoria) VALUES(?,?,?,?,?,?)";
    
                    $stmt = $db->prepare($query);
                    $stmt->bindParam(1 ,$data->nombre_producto ,PDO::PARAM_STR);
                    $stmt->bindParam(2 ,$data->precio ,PDO::PARAM_INT);
                    $stmt->bindParam(3 ,$data->poster ,PDO::PARAM_STR);
                    $stmt->bindParam(4 ,$data->stock ,PDO::PARAM_INT);
                    $stmt->bindParam(5 ,$data->descripcion ,PDO::PARAM_STR);
                    $stmt->bindParam(6 ,$data->cod_categoria ,PDO::PARAM_INT);
    
                    if($stmt->execute()){
                        http_response_code(200);
                        echo json_encode(
                            array(
                                "message" => "successful query"
                            )
                        );
                    }else{
                        http_response_code(400);
                        echo json_encode(
                            array(
                                "message" => "no successful query"
                            )
                        );
                    }

                }else{
                    http_response_code(401);
                    echo json_encode(
                        array(
                            "message" => "Denied Access"
                        )
                    );
                }

            }else{
                http_response_code(401);
                echo json_encode(
                    array(
                        "message" => "Denied Access"
                    )
                );
            }

        }

    }


    public function inactiveProduct($request){

        if($request->getMethod() == 'PATCH'){

            $token = $this->getToken($request);
            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'tiendas'){

                    $id = $request->getAttribute('id');
                    $estado = false;
    
                    $query = "UPDATE productos SET estado = ?
                                WHERE cod_producto = ?";
    
                    $database = new Database();
                    $db = $database->getConnection();
    
                    $stmt = $db->prepare($query);
                    
                    $stmt->bindParam(1 , $estado , PDO::PARAM_BOOL);
                    $stmt->bindParam(2 , $id , PDO::PARAM_INT);
    
                    if($stmt->execute()){
                        echo json_encode(
                            array(
                                "message" => "successful query"
                            )
                        );
                        
                    }else{
                        echo json_encode(
                            array(
                                "message" => "no successful query"
                            )
                        );
                    }

                }else{
                    http_response_code(401);             
                    echo json_encode(array("message" => "Access denied."));    
                }                

            }else{ 
                http_response_code(401);             
                echo json_encode(array("message" => "Access denied."));
            }
            
        }

    }

    public function actualizarProducto($request){

        if($request->getMethod() == 'PATCH'){

            $token = $this->getToken($request);

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'tiendas'){
                    
                    $id = $request->getAttribute('id');
                    $data = json_decode(file_get_contents("php://input"));
                    
                    $database = new Database();
                    $db = $database->getConnection();
                    
            
                    $query = "UPDATE productos
                                SET nombre_producto = ? , precio = ?, stock = ?, 
                                    cod_categoria = ? , descripcion = ?, poster= ?
                                WHERE cod_producto = ? ";
            
                    $stmt = $db->prepare($query);
                        
                    $stmt->bindParam(1 , $data->nombre_producto , PDO::PARAM_STR);
                    $stmt->bindParam(2 , $data->precio , PDO::PARAM_INT);
                    $stmt->bindParam(3 , $data->stock , PDO::PARAM_INT);
                    $stmt->bindParam(4 , $data->cod_categoria , PDO::PARAM_STR);
                    $stmt->bindParam(5 , $data->descripcion , PDO::PARAM_STR);
                    $stmt->bindParam(6 , $data->poster , PDO::PARAM_STR);
                    $stmt->bindParam(7 , $id , PDO::PARAM_INT);
            
                    if($stmt->execute()){
                        http_response_code(200);
                        echo json_encode(
                            array(
                                "message" => "successful query"
                            )
                        );
                    }else{
                        http_response_code(400);
                        echo json_encode(
                            array(
                                "message" => "no successful query"
                            )
                        );
                    }

                }else{
                    http_response_code(401);
                    echo json_encode(
                        array(
                            "message" => "Denied Access"
                        )
                    );
                }                

            }else{
                http_response_code(401);
                echo json_encode(
                    array(
                        "message" => "Denied Access"
                    )
                );
            }

        }

    }


}
?>
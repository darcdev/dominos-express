<?php

namespace Controllers;

use Config\Database;
use Models\Repartidor;
use Auth\Auth;
use PDO;


class RepartidorController extends BaseUserController {

    public $model_name = "Models\Repartidor";

    public function createRepartidor($request){
        
        if($request->getMethod() == 'POST'){
            
            $data = json_decode(file_get_contents("php://input"));
    
            $repartidor = new Repartidor();
            $email_exists = $repartidor->emailExists($data->correo);


                if(!$email_exists){

                    $database = new Database();
                    $db = $database->getConnection();
                    $id_rol = 3;
        
                    $repartidor->setNombre($data->nombre);
                    $repartidor->setApellido($data->apellido);
                    $repartidor->setCorreo($data->correo);
                    $repartidor->setPassword($data->password);
        
                    $query = "INSERT INTO " . $repartidor->table_name . "(nombre , apellido , correo , password, id_rol) VALUES(? , ? , ? , ?, ?)";
                    $stmt = $db->prepare($query);
                    $stmt->bindParam(1, $data->nombre , PDO::PARAM_STR);
                    $stmt->bindParam(2, $data->apellido , PDO::PARAM_STR);
                    $stmt->bindParam(3, $data->correo , PDO::PARAM_STR);
                
                    $password_hash = password_hash($data->password, PASSWORD_BCRYPT);
                    $stmt->bindParam(4, $password_hash , PDO::PARAM_STR);
                    $stmt->bindParam(5, $id_rol , PDO::PARAM_INT);
                    
    
                    if($stmt->execute()){
                        http_response_code(200);
                     
                        echo json_encode(array("message" => "repartidor was created."));
    
                    }else{
                        http_response_code(401);
                        echo json_encode(array("message" => "Unable to create repartidor."));
                    }


                }else {
                    echo json_encode(array("message" => "Email already exists"));
                }
                
            

        }
    }

    public function updateRepartidor($request){

        if($request->getMethod() == 'PATCH'){

            $token = $this->getToken($request);

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            var_dump($verified_token);
            if($verified_token){

                $rol = $this->getRol($token);
                $repartidor = new Repartidor();
                
                if($rol == 'repartidores'){
                    
                    $id = $auth->getData($token)->id;
                    $data = json_decode(file_get_contents("php://input"));
                    
                    $email_exists = $repartidor->emailExists($data->correo);

                    if(!$email_exists){

                        $database = new Database();
                        $db = $database->getConnection();
                
                        $query = "UPDATE repartidores
                                    SET nombre = ? , apellido = ? , correo = ? , password = ? 
                                    WHERE id = ? ";
                
                        $stmt = $db->prepare($query);
                            
                        $stmt->bindParam(1 , $data->nombre , PDO::PARAM_STR);
                        $stmt->bindParam(2 , $data->apellido , PDO::PARAM_STR);
                        $stmt->bindParam(3 , $data->correo , PDO::PARAM_STR);
                        $stmt->bindParam(4 , $data->password , PDO::PARAM_STR);
                        $stmt->bindParam(5 , $id , PDO::PARAM_INT);
                        var_dump($stmt->execute());
                        if($stmt->execute()){
                            http_response_code(200);
                            echo json_encode(
                                array(
                                    "message" => "successful query"
                                )
                            );
                        }else{
                            http_response_code(400);
                            echo json_encode(
                                array(
                                    "message" => "no successful query"
                                )
                            );
                        }

                    }else{
                        echo json_encode(
                            array(
                                "message" => "Email already exists"
                            )
                        );
                    }
                    

                }else{
                    http_response_code(401);
                    echo json_encode(
                        array(
                            "message" => "Denied Access"
                        )
                    );
                }

            }else{
                http_response_code(401);
                echo json_encode(
                    array(
                        "message" => "Denied Access"
                    )
                );
            }

        }

    }

    
    public function obtenerPedidosActivos($request){

        if($request->getMethod() == 'GET'){

            
            $token = $this->getToken($request);
            

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);

            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'repartidores'){
                    
                    $database = new Database();
                    $db = $database->getConnection();
                    
                    $id = $auth->getData($token)->id;

                    $query = "SELECT p.cod_pedido, p.estado, p.hora_entrega, p.hora_pedido, p.fecha_pedido,
                                     c.id id_cliente, c.nombre nombre_cliente, c.apellido apellido_cliente, c.direccion,
                                     r.id id_repartidor, r.nombre nombre_repartidor, r.apellido apellido_repartidor,
                                     t.id id_tienda, t.nombre nombre_tienda
                              FROM pedidos AS p
                                JOIN clientes AS c ON c.id = p.id_cliente
                                JOIN repartidores AS r ON r.id = p.id_repartidor
                                JOIN registra AS reg ON reg.cod_pedido = p.cod_pedido
                                JOIN tiendas AS t ON t.id = reg.id_tienda
                              WHERE p.estado = true AND r.id = ?
                              ORDER BY p.fecha_pedido, p.hora_pedido DESC";

    
                    $stmt = $db->prepare($query);
                    $stmt->bindParam(1 , $id , PDO::PARAM_INT);
                    $stmt->execute();
                    $num = $stmt->rowCount();
                    #echo $num;
                    if($num > 0){
                        $pedidos = $stmt->fetchAll(PDO::FETCH_OBJ);
                        
                        echo json_encode(
                            array(
                                "message" => "successful query",
                                "pedidos" => $pedidos
                            )
                        );

                    }

                }else{
                    http_response_code(401);             
                    echo json_encode(array("message" => "Access denied."));    
                }    

            }else{ 
                http_response_code(401);             
                echo json_encode(array("message" => "Access denied."));
            }
            
        }

    }

    public function pedidoEntregado($request){

        if($request->getMethod() == 'PATCH'){

            $token = $this->getToken($request);

            $auth = new Auth();
            $verified_token = $auth->validateJWT($token);
            
            if($verified_token){

                $rol = $this->getRol($token);

                if($rol == 'repartidores'){
                    
                    $id = $request->getAttribute('id');
                    $data = json_decode(file_get_contents("php://input"));
        
                    $database = new Database();
                    $db = $database->getConnection();
            
                    $estado = true;
    
                    $query = "UPDATE pedidos
                                SET estado = ? , hora_entrega = (SELECT current_time)
                                WHERE cod_pedido = ? ";
            
                    $stmt = $db->prepare($query);
                        
                    $stmt->bindParam(1 , $estado , PDO::PARAM_BOOL);
                    $stmt->bindParam(2 , $id , PDO::PARAM_INT);
                    
                    if($stmt->execute()){
                        http_response_code(200);
                        echo json_encode(
                            array(
                                "message" => "successful query"
                            )
                        );
                    }else{
                        http_response_code(400);
                        echo json_encode(
                            array(
                                "message" => "no successful query"
                            )
                        );
                    }

                }else{
                    http_response_code(401);
                    echo json_encode(
                        array(
                            "message" => "Denied Access"
                        )
                    );
                }                           

            }else{
                http_response_code(401);
                echo json_encode(
                    array(
                        "message" => "Denied Access"
                    )
                );
            }

            
        }

    }

}
?>
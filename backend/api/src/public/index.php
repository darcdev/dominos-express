<?php


if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PATCH");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

ini_set('display_errors', 1);
ini_set('display_starup_error', 1);
error_reporting(E_ALL);

require_once '../vendor/autoload.php';

use Aura\Router\RouterContainer;

$request = Laminas\Diactoros\ServerRequestFactory::fromGlobals(
    $_SERVER,
    $_GET,
    $_POST,
    $_COOKIE,
    $_FILES
);


$routerContainer = new RouterContainer();
$map = $routerContainer->getMap();


/* RECURSOS GENERALES */
$map->patch('delete-user', '/proyectodb/api/delete/{user}/{id}', [
    'controller' => 'Controllers\BaseUserController',
    'action' => 'inactiveUser'
]);

$map->get('ver-producto', '/proyectodb/api/info/productos/{id}', [
    'controller' => 'Controllers\BaseUserController',
    'action' => 'getProduct'
]);

$map->get('pedidos', '/proyectodb/api/{user}/pedidos/{estado}', [
    'controller' => 'Controllers\BaseUserController',
    'action' => 'obtenerPedidosPorEstado'
]);

$map->get('tiendas-categoria', '/proyectodb/api/tiendas/categoria/{nombre_cat}', [
    'controller' => 'Controllers\BaseUserController',
    'action' => 'tiendasPorCategoria'
]);

$map->get('productos-tienda', '/proyectodb/api/tiendas/{nombre_tienda}/productos/categorias/{categoria_producto}', [
    'controller' => 'Controllers\BaseUserController',
    'action' => 'obtenerProductosPorCategoriaTienda'
]);

$map->get('categoria-tienda', '/proyectodb/api/tiendas/categorias/{nombre_tienda}', [
    'controller' => 'Controllers\BaseUserController',
    'action' => 'obtenerCategoriaTienda'
]);


/* RECURSOS PARA LOS USUARIOS CLIENTES */
$map->post('signup-cliente', '/proyectodb/api/crear/clientes', [
    'controller' => 'Controllers\ClientController',
    'action' => 'createClient'
]);

$map->patch('actualizar-cliente', '/proyectodb/api/clientes/me/actualizar', [
    'controller' => 'Controllers\ClientController',
    'action' => 'updateClient'
]);

$map->post('login-cliente', '/proyectodb/api/login/clientes', [
    'controller' => 'Controllers\ClientController',
    'action' => 'login'
]);


$map->get('info-cliente', '/proyectodb/api/info/clientes/me', [
    'controller' => 'Controllers\ClientController',
    'action' => 'personalInfo'
]);

$map->post('ordenar-pedido', '/proyectodb/api/clientes/ordenar/pedido', [
    'controller' => 'Controllers\ClientController',
    'action' => 'ordenarPedido'
]);

$map->get('categorias-productos-tienda', '/proyectodb/api/tiendas/{nombre_tienda}/productos/categorias', [
    'controller' => 'Controllers\ClientController',
    'action' => 'categoriasProductosPorTienda'
]);



/* RECURSOS PARA LOS USUARIOS REPARTIDORES */
$map->post('signup-repartidor', '/proyectodb/api/crear/repartidores', [
    'controller' => 'Controllers\RepartidorController',
    'action' => 'createRepartidor'
]);

$map->patch('actualizar-repartidor', '/proyectodb/api/repartidores/actualizar/me', [
    'controller' => 'Controllers\RepartidorController',
    'action' => 'updateRepartidor'
]);

$map->post('login-repartidor', '/proyectodb/api/login/repartidores', [
    'controller' => 'Controllers\RepartidorController',
    'action' => 'login'
]);

$map->get('info-repartidor', '/proyectodb/api/repartidores/info/me', [
    'controller' => 'Controllers\RepartidorController',
    'action' => 'personalInfo'
]);

$map->get('pedidos-activos', '/proyectodb/api/repartidores/me/pedidos/activos', [
    'controller' => 'Controllers\RepartidorController',
    'action' => 'obtenerPedidosActivos'
]);

$map->patch('pedido-entregado', '/proyectodb/api/repartidores/pedidos/{id}/entregado', [
    'controller' => 'Controllers\RepartidorController',
    'action' => 'pedidoEntregado'
]);



/* RECURSOS PARA LOS USUARIOS TIENDAS */
$map->post('signup-tienda', '/proyectodb/api/crear/tiendas', [
    'controller' => 'Controllers\MarketController',
    'action' => 'createMarket'
]);

$map->get('info-tienda', '/proyectodb/api/tiendas/info/me', [
    'controller' => 'Controllers\MarketController',
    'action' => 'personalInfo'
]);

$map->patch('actualizar-tienda', '/proyectodb/api/tiendas/actualizar/me', [
    'controller' => 'Controllers\MarketController',
    'action' => 'updateMarket'
]);

$map->post('login-tienda', '/proyectodb/api/login/tiendas', [
    'controller' => 'Controllers\MarketController',
    'action' => 'login'
]);

$map->post('agregar-producto', '/proyectodb/api/tiendas/crear/producto', [
    'controller' => 'Controllers\MarketController',
    'action' => 'agregarProducto'
]);

$map->patch('actualizar-producto', '/proyectodb/api/tiendas/productos/actualizar/{id}', [
    'controller' => 'Controllers\MarketController',
    'action' => 'actualizarProducto'
]);

$map->patch('delete-product', '/proyectodb/api/tiendas/productos/delete/{id}', [
    'controller' => 'Controllers\MarketController',
    'action' => 'inactiveProduct'
]);





/* RECURSOS PARA LOS USUARIOS ADMINISTRADORES */

$map->post('login-admin', '/proyectodb/api/login/administradores', [
    'controller' => 'Controllers\AdminController',
    'action' => 'login'
]);

$map->patch('actualizar-admin', '/proyectodb/api/administradores/actualizar/{id}', [
    'controller' => 'Controllers\AdminController',
    'action' => 'updateAdmin'
]);

$map->get('info-admin', '/proyectodb/api/info/administradores/{id}', [
    'controller' => 'Controllers\AdminController',
    'action' => 'personalInfo'
]);

$map->post('agregar-categoria-tienda', '/proyectodb/api/crear/categoria/tienda', [
    'controller' => 'Controllers\AdminController',
    'action' => 'agregarCategoriaTienda'
]);

$map->post('agregar-categoria-producto', '/proyectodb/api/crear/categoria/producto', [
    'controller' => 'Controllers\AdminController',
    'action' => 'agregarCategoriaProducto'
]);

$map->patch('actualizar-categoria-tienda', '/proyectodb/api/actualizar/categoria/tienda/{id}', [
    'controller' => 'Controllers\AdminController',
    'action' => 'actualizarCategoriaTienda'
]);

$map->get('repartidores', '/proyectodb/api/repartidores/{id}', [
    'controller' => 'Controllers\AdminController',
    'action' => 'obtenerRepartidor'
]);







$matcher = $routerContainer->getMatcher();
$route = $matcher->match($request);

#var_dump($route);

if (!$route) {
    echo 'No route';
} else {

    foreach ($route->attributes as $key => $val) {
        $request = $request->withAttribute($key, $val);
    }

    $handlerData = $route->handler;
    $controllerName = $handlerData['controller'];
    $actionName = $handlerData['action'];

    $controller = new $controllerName;
    $response = $controller->$actionName($request);

}

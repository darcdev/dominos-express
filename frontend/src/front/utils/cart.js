const getItem = (id, products) => {
  const product = products.find((item) => item.id === id);
  return product;
};

const getTotal = (products) => {
  let total = 0;
  products.map((item) => (total += item.total));
  return total;
};

export { getItem, getTotal };

class CartProducts {
  constructor() {}

  createCart() {
    window.localStorage.setItem("cart", JSON.stringify([]));
  }

  getItems() {
    var products = JSON.parse(window.localStorage.getItem("cart"));
    return products;
  }
}

export default CartProducts;

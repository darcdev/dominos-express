import axios from "axios";
import { API_URL } from "../../../config";
import { set_cookie } from "../../front/utils/cookies";

const loginRequest = (payload) => ({
  type: "LOGIN_REQUEST",
  payload,
});

const logoutRequest = (payload) => ({
  type: "LOGOUT_REQUEST",
  payload,
});

const registerRequest = (payload) => ({
  type: "REGISTER_REQUEST",
  payload,
});

const addProductCartRequest = (payload) => ({
  type: "ADD_PRODUCT_CART",
  payload,
});
const emptyBasket = (payload) => ({
  type: "CART_EMPTY_REQUEST",
  payload,
});

const updateProduct = (payload) => ({
  type: "UPDATE_PRODUCT",
  payload,
});

const incrementProduct = (id) => {
  let tmpCart = [...JSON.parse(window.localStorage.getItem("cart"))];
  let selectedProudct = tmpCart.find((item) => item.id === id);
  const indexProduct = tmpCart.indexOf(selectedProudct);
  const product = tmpCart[indexProduct];

  if (product.cant < product.stock) {
    product.cant = product.cant + 1;
    product.total = product.cant * product.price;
  }

  return (dispatch) => {
    dispatch(updateProduct([...tmpCart]));
  };
};

const decrementProduct = (id) => {
  let tmpCart = [...JSON.parse(window.localStorage.getItem("cart"))];
  let selectedProudct = tmpCart.find((item) => item.id === id);
  const indexProduct = tmpCart.indexOf(selectedProudct);
  const product = tmpCart[indexProduct];

  product.cant = product.cant - 1;
  if (product.cant === 0) {
    tmpCart = removeItem(id, tmpCart);
  } else {
    product.total = product.cant * product.price;
  }
  return (dispatch) => {
    dispatch(updateProduct([...tmpCart]));
  };
};

const removeItem = (id, tmpCart) => {
  tmpCart = tmpCart.filter((item) => {
    return item.id !== id;
  });

  return tmpCart;
};

const addProductCart = (data) => {
  const product = {
    ...data,
    cant: 1,
    note: "",
    total: data.price,
  };
  return (dispatch) => {
    dispatch(addProductCartRequest(product));
  };
};

const registerUser = ({ email, password }, redirectUrl) => {
  return (dispatch) => {
    dispatch(registerRequest({}));
  };
};

const loginUser = ({ email, password }, user) => {
  return async (dispatch) => {
    let definitiveUser = "";
    if (user == "cliente") {
      definitiveUser = "clientes";
    } else if (user == "repartidor") {
      definitiveUser = "repartidores";
    } else if (user == "admin") {
      definitiveUser = "administradores";
    } else if (user == "tienda") {
      definitiveUser = "tiendas";
    }

    const url = `${API_URL}/api/login/${definitiveUser}`;
    const data = Buffer.from(`${email}:${password}`, "utf8").toString("base64");
    console.log(definitiveUser);
    await axios
      .post(
        url,
        {
          tipo_usuario: definitiveUser,
        },
        {
          headers: {
            Authorization: `Basic ${data}`,
          },
        }
      )
      .then(({ data: auth }) => {
        console.log(auth);
        set_cookie("token", auth.jwt);
        set_cookie("email", auth.user.correo);

        if (auth.user.rol == "clientes") {
          set_cookie("role", "usuario");
        } else {
          set_cookie("role", auth.user.rol);
        }

        document.getElementById("validation-user").style.display = "none";
        console.log(user);
        if (user == "cliente") {
          window.location.href = "/";
        } else if (user == "repartidor") {
          window.location.href = `/repartidor/${auth.user.id}?action=pedidos-pendientes`;
        }
      })
      .catch((e) => {
        document.getElementById("validation-user").style.display = "block";
      });

    dispatch(loginRequest({ email, password, rol: "usuario" }));
  };
};

export {
  loginUser,
  registerUser,
  logoutRequest,
  addProductCart,
  emptyBasket,
  incrementProduct,
  decrementProduct,
};

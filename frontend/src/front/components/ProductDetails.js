import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { getItem } from "../utils/cart";
import Container from "react-bootstrap/Container";
import { LinkContainer } from "react-router-bootstrap";
import { connect } from "react-redux";
import { incrementProduct, decrementProduct, addProductCart } from "../actions";
const customStyles = {
  overlay: { zIndex: 2000 },
};

const ProductDetails = function ProductDetails({
  show,
  handleClose,
  addProduct,
  incrementProduct,
  decrementProduct,
  products,
  addProductCart,
}) {
  const [productDetails, setProductDetails] = useState({
    notes: "",
  });

  const handleInput = (event) => {
    setProductDetails({
      ...productDetails,
      [event.target.name]: event.target.value,
    });
  };
  const addProductToCart = (value) => {
    if (!getItem(value.id, products)) {
      addProductCart(value);
    }
  };
  const handleIncrement = (id) => {
    incrementProduct(id);
  };
  const handleDecrement = (id) => {
    decrementProduct(id);
  };

  return (
    <Modal style={customStyles} size="xl" show={show} onHide={handleClose}>
      <Modal.Header closeButton></Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <LinkContainer
                to="/tiendas/tipo/hogar"
                className="category-over-effect"
              >
                <div
                  className="card border-0 rounded"
                  style={{ width: "12rem" }}
                >
                  <img
                    src="https://images.rappi.cl/store_type/hogar-1601559447.png?d=200x200&e=webp"
                    className="card-img-top"
                    alt="..."
                  />
                </div>
              </LinkContainer>
            </Col>
            <Col md={6}>
              <Row>
                <Col md={12} className="mb-2">
                  {console.log(addProduct)}
                  <h2>{addProductCart.nombre}</h2>
                </Col>
                <Col md={12} className="mb-4">
                  <p style={{ fontSize: "1.1rem" }} className="text-muted ">
                    Alimento Lácteo Finesse Mora Botella X 1000G PLU:954412
                  </p>
                </Col>
                <Col md={12} className="mb-5">
                  <b style={{ fontSize: "1.3rem" }}>$5850</b>
                </Col>
                <Col md={12} className="mb-4">
                  <h5>Agregar instrucciones adicionales</h5>
                </Col>

                <Col md={12} className="mb-4">
                  <div className="form-group">
                    <textarea
                      className="form-control"
                      placeholder="Escribir comentarios Adicionales"
                      rows="4"
                      id="notes"
                      name="notes"
                      onChange={handleInput}
                    ></textarea>
                  </div>

                  <p>
                    <b>Nota:</b> En este campo no se aceptan modificaciones que
                    generen valor adicional en la factura
                  </p>
                </Col>
                <Col md={12}>
                  {getItem(addProduct.id, products) ? (
                    <div
                      className="cont-info-simulate"
                      style={{ width: "200px", margin: "0 auto" }}
                    >
                      <button
                        onClick={() => handleDecrement(addProduct.id)}
                        className="mr-4"
                      >
                        {getItem(addProduct.id, products).cant === 1 ? (
                          <img
                            style={{ width: "15px" }}
                            src="https://img.icons8.com/android/24/000000/trash.png"
                            alt=""
                          />
                        ) : (
                          "-"
                        )}{" "}
                      </button>
                      {getItem(addProduct.id, products).cant}{" "}
                      <button
                        onClick={() => handleIncrement(addProduct.id)}
                        className="ml-3"
                      >
                        {" "}
                        {getItem(addProduct.id, products).cant <
                        addProduct.stock
                          ? "+"
                          : ""}
                      </button>
                    </div>
                  ) : (
                    <button
                      onClick={() => addProductToCart(addProduct)}
                      style={{
                        fontSize: "0.8rem",
                        backgroundColor: "#3f51b5",
                      }}
                      type="button"
                      className="btn btn-block mt-0 btn-primary"
                      data-toggle="button"
                      aria-pressed="false"
                    >
                      Agregar
                    </button>
                  )}
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
    </Modal>
  );
};

const mapStateToProps = (state) => {
  return {
    products: state.cart.products,
  };
};
const mapDispatchToProps = {
  incrementProduct,
  decrementProduct,
  addProductCart,
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductDetails);

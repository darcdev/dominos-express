import React from "react";

const WithoutProducts = function WithoutProducts() {
  return (
    <div className="row pt-2 align-items-center justify-content-center basket-shop-zone">
      <div className="col-10 ">
        <p className="text-center">
          <img
            src="https://img.icons8.com/nolan/64/add-basket.png"
            alt="basket shop"
          />
        </p>
        <h4 className="mb-3 text-center text-bold text-muted">
          Tu canasta está vacía
        </h4>
        <p className="text-center text-muted ">
          Si deseas puedes regresar a nuestras tiendas y agregar productos a tu
          canasta
        </p>
      </div>
    </div>
  );
};

export default WithoutProducts;

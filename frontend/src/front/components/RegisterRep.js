import React, { useState } from "react";
import { Link } from "react-router-dom";
import "../assets/styles/components/Login.css";
const Register = function Register() {
  const [form, setForm] = useState({
    username: "",
    lastname: "",
    email: "",
    password: "",
    validation: false,
  });

  const handleInput = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };

  const handleValidation = () => {
    const { username, lastname, email, password } = form;
    if (!(username && lastname && email && password)) {
      setForm({
        ...form,
        validation: true,
      });
    } else {
      setForm({
        ...form,
        validation: false,
      });
    }
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    handleValidation();
  };
  return (
    <div className="limiter">
      <div className="container-login100">
        <div className="wrap-login100">
          <div className="login100-form-title">
            <span className="login100-form-title-1">Registro Repartidor</span>
          </div>

          <form className="login100-form validate-form">
            <div
              className="mb-4 wrap-input100 validate-input m-b-26"
              data-validate="nombre es requerido"
            >
              <span className="label-input100">Nombre</span>
              <input
                className="input100"
                type="text"
                name="username"
                value={form.username}
                placeholder="Digite su primer nombre"
                onChange={handleInput}
              />
              <span className="focus-input100"></span>
            </div>
            <div
              className="mb-4 wrap-input100 validate-input m-b-26"
              data-validate="apellido es requerido"
            >
              <span className="label-input100">Apellido</span>
              <input
                className="input100"
                type="text"
                name="lastname"
                value={form.lastname}
                placeholder="Digite su primer Apellido"
                onChange={handleInput}
              />
              <span className="focus-input100"></span>
            </div>
            <div
              className="mb-4 wrap-input100 validate-input m-b-26"
              data-validate="Correo es requerido"
            >
              <span className="label-input100">Correo</span>
              <input
                className="input100"
                type="email"
                name="email"
                placeholder="Digite su correo"
                value={form.email}
                onChange={handleInput}
              />
              <span className="focus-input100"></span>
            </div>
            <div
              className="mb-4 wrap-input100 validate-input m-b-18"
              data-validate="contraseña is requerida"
            >
              <span className="label-input100">Contraseña</span>
              <input
                className="input100"
                type="password"
                name="password"
                value={form.password}
                placeholder="Digite su contraseña"
                onChange={handleInput}
              />
              <span className="focus-input100"></span>
            </div>

            {form.validation ? (
              <div className="mb-4 mb-3" style={{ color: "red" }}>
                Por favor rellene todos los campos
              </div>
            ) : (
              ""
            )}

            <div className="mb-4 container-login100-form-btn">
              <button className="login100-form-btn" onClick={handleSubmit}>
                Registrarse
              </button>
            </div>

            <div>
              <small>Ya tienes una cuenta ? </small>
              <Link to="/loguear?usuario=repartidor">Iniciar Sesión</Link>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Register;

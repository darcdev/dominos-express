import React, { useState } from "react";
import clsx from "clsx";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import StorefrontIcon from "@material-ui/icons/Storefront";
import MotorcycleIcon from "@material-ui/icons/Motorcycle";
import FastfoodIcon from "@material-ui/icons/Fastfood";
import LocalPharmacyIcon from "@material-ui/icons/LocalPharmacy";
import StoreIcon from "@material-ui/icons/Store";
import LocalBarIcon from "@material-ui/icons/LocalBar";
import WorkIcon from "@material-ui/icons/Work";
import AssignmentTurnedInIcon from "@material-ui/icons/AssignmentTurnedIn";
import MoreHorizIcon from "@material-ui/icons/MoreHoriz";
import CategoryPanel from "./CategoryPanel";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  list: {
    width: 250,
  },
  fullList: {
    width: "auto",
  },
  paper: {
    height: "calc(100% - 78px)",
    top: 70,
  },
  BackdropProps: {
    backgroundColor: "rgba(0, 0, 0, 0)",
  },
});

export default function TemporaryDrawer({ toggleDrawer, drawerOpenState }) {
  const classes = useStyles();
  const [activePanel, setActivePanel] = useState(false);

  const toggleCategoryPanel = () => {
    setActivePanel(!activePanel);
  };

  const list = () => (
    <div
      className={clsx(classes.list, {
        [classes.fullList]: false,
      })}
      role="presentation"
      onClick={toggleDrawer(false)}
      onKeyDown={toggleDrawer(false)}
    >
      <List>
        <ListItem>
          <ListItemText primary={"Categorias"} />
        </ListItem>
      </List>
      <Divider />
      <List>
        <ListItem button>
          <ListItemIcon>
            <FastfoodIcon />
          </ListItemIcon>
          <Link style={{ color: "black" }} to="/tiendas/tipo/supermercados">
            <ListItemText primary={"Supermercados"} />
          </Link>
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <StoreIcon />
          </ListItemIcon>
          <Link style={{ color: "black" }} to="/tiendas/tipo/restaurantes">
            <ListItemText primary={"Restaurantes"} />
          </Link>
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <LocalPharmacyIcon />
          </ListItemIcon>
          <Link style={{ color: "black" }} to="/tiendas/tipo/farmacias">
            <ListItemText primary={"Farmacias"} />
          </Link>
        </ListItem>

        <ListItem button>
          <ListItemIcon>
            <LocalBarIcon />
          </ListItemIcon>
          <Link style={{ color: "black" }} to="/tiendas/tipo/bebidas">
            <ListItemText primary={"Bebidas"} />
          </Link>
        </ListItem>

        <ListItem button>
          <ListItemIcon>
            <MoreHorizIcon />
          </ListItemIcon>
          <ListItemText onClick={toggleCategoryPanel} primary={"Ver mas"} />
        </ListItem>
      </List>
      <Divider />

      <List>
        <ListItem>
          <ListItemText primary={"Ingresar"} />
        </ListItem>
      </List>
      <Divider />
      <List>
        <ListItem button>
          <ListItemIcon>
            <StorefrontIcon />
          </ListItemIcon>
          <Link style={{ color: "black" }} to="/loguear?usuario=tienda">
            <ListItemText primary={"DomiTienda"} />
          </Link>
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <MotorcycleIcon />
          </ListItemIcon>
          <Link style={{ color: "black" }} to="/loguear?usuario=repartidor">
            <ListItemText primary={"DomiTendero"} />
          </Link>
        </ListItem>
      </List>
      <Divider />

      <List>
        <ListItem button>
          <ListItemIcon>
            <AssignmentTurnedInIcon />
          </ListItemIcon>
          <Link style={{ color: "black" }} to="/registrar?usuario=tienda">
            <ListItemText primary={"Registrar Tienda"} />
          </Link>
        </ListItem>
        <ListItem button>
          <ListItemIcon>
            <WorkIcon />
          </ListItemIcon>
          <Link style={{ color: "black" }} to="/registrar?usuario=repartidor">
            <ListItemText primary={"Trabaja con nosotros"} />
          </Link>
        </ListItem>
      </List>
    </div>
  );

  return (
    <div>
      <Drawer
        style={{ zIndex: 1000 }}
        ModalProps={{
          BackdropProps: {
            classes: {
              root: classes.BackdropProps,
            },
          },
        }}
        anchor="left"
        classes={{ paper: classes.paper }}
        open={drawerOpenState}
        onClose={toggleDrawer(false, 1)}
      >
        {list()}
      </Drawer>

      <div className="row">
        <div className="col-12">
          <CategoryPanel
            activePanel={activePanel}
            handleClose={toggleCategoryPanel}
          />
        </div>
      </div>
    </div>
  );
}

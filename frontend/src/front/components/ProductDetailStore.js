/* eslint-disable jsx-a11y/no-onchange */
import React, { useState, useEffect } from "react";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import { LinkContainer } from "react-router-bootstrap";

const customStyles = {
  overlay: { zIndex: 2000 },
};

const ProductDetailStore = function ProductDetailStore({
  activeProduct,
  handleClose,
  idProduct,
}) {
  const [productDetail, setProductDetail] = useState({
    price: "",
    cant: "",
    category: "",
    description: "",
    image: "",
    name: "",
    imageFile: "",
    validation: false,
  });
  const [categoriesProduct, setCategoriesProduct] = useState([]);

  useEffect(() => {
    const categories = ["supermercados", "relojeria", "hogar"];
    setCategoriesProduct(categories);
  }, []);

  useEffect(() => {
    const productData = {
      price: 2000,
      cant: 10,
      categoryProduct: "supermercados",
      description: "esta es una descripcion",
      image:
        "https://images.rappi.cl/store_type/hogar-1601559447.png?d=200x200&e=webp",
      name: "Pan bimbo x12",
    };
    setProductDetail({ ...productDetail, ...productData });
  }, []);

  const handleImage = (event) => {
    var file = event.target.files[0];
    setProductDetail({ ...productDetail, imageFile: file });
  };
  const handleInput = (event) => {
    setProductDetail({
      ...productDetail,
      [event.target.name]: event.target.value,
    });
  };

  const handleValidation = () => {
    const { price, cant, description, name } = productDetail;
    if (!(price && cant && description && name)) {
      setProductDetail({
        ...productDetail,
        validation: true,
      });
    } else {
      setProductDetail({
        ...productDetail,
        validation: false,
      });
    }
  };
  const handleSubmit = () => {
    handleValidation();
    //envio de datos
  };
  return (
    <Modal
      style={customStyles}
      size="xl"
      show={activeProduct}
      onHide={handleClose}
    >
      <Modal.Header closeButton></Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={6}>
              <LinkContainer
                to="/tiendas/tipo/hogar"
                className="category-over-effect"
              >
                <div
                  className="card border-0 rounded"
                  style={{ width: "12rem" }}
                >
                  <img
                    src={productDetail.image}
                    className="card-img-top mb-3"
                    alt="..."
                  />
                </div>
              </LinkContainer>
              <div className="custom-file text-left" style={{ width: "80%" }}>
                <input
                  type="file"
                  className="custom-file-input"
                  id="file"
                  name="file"
                  onChange={handleImage}
                />
                <label className="custom-file-label" htmlFor="file">
                  Seleccionar Imagen Producto
                </label>
              </div>
            </Col>
            <Col md={6}>
              <Row>
                <Col md={12}>
                  <form>
                    <div className="form-group">
                      <label htmlFor="name">nombre</label>
                      <input
                        type="text"
                        className="form-control"
                        id="name"
                        placeholder=""
                        name="name"
                        value={productDetail.name}
                        onChange={handleInput}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="price">Precio</label>
                      <input
                        type="number"
                        className="form-control"
                        id="price"
                        placeholder=""
                        name="price"
                        value={productDetail.price}
                        onChange={handleInput}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="cant">cantidad</label>
                      <input
                        type="number"
                        className="form-control"
                        id="cant"
                        name="cant"
                        value={productDetail.cant}
                        placeholder=""
                        onChange={handleInput}
                      />
                    </div>
                    <div className="form-group">
                      <label htmlFor="categoryProduct">Categoria</label>
                      <select
                        name="categoryProduct"
                        onChange={handleInput}
                        className="form-control"
                        id="categoryProduct"
                      >
                        {categoriesProduct.map((value, index) => {
                          return (
                            <option
                              defaultValue={value == productDetail.category}
                              key={index}
                              value={value}
                            >
                              {value}
                            </option>
                          );
                        })}
                      </select>
                    </div>

                    <div className="form-group mb-5">
                      <label htmlFor="description">Descripción</label>
                      <textarea
                        className="form-control"
                        id="description"
                        name="description"
                        rows="3"
                        value={productDetail.description}
                        onChange={handleInput}
                      ></textarea>
                    </div>
                  </form>
                </Col>

                <Col md={12}>
                  {productDetail.validation ? (
                    <div className="mb-4 mb-3" style={{ color: "red" }}>
                      Por favor rellene todos los campos
                    </div>
                  ) : (
                    ""
                  )}
                  <button
                    onClick={handleSubmit}
                    style={{
                      width: "40%",
                      fontSize: "1rem",
                    }}
                    type="button"
                    className="btn  mt-0 mr-4 btn-success"
                    data-toggle="button"
                    aria-pressed="false"
                  >
                    Actualizar
                  </button>
                  <button
                    onClick={handleClose}
                    style={{
                      width: "40%",
                      fontSize: "1rem",
                    }}
                    type="button"
                    className="btn  mt-0 btn-danger"
                    data-toggle="button"
                    aria-pressed="false"
                  >
                    Cancelar
                  </button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
    </Modal>
  );
};

export default ProductDetailStore;

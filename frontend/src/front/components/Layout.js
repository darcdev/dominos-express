import React from "react";
import { connect } from "react-redux";
import Header from "../components/Header";
import Footer from "./Footer";

const Layout = function Layout({ rol, children, reload }) {
  return (
    <>
      <div id="header-container" className="sticky-top">
        <Header reload={reload} />
      </div>
      {children}
      {rol == "usuario" || rol == "home" ? <Footer /> : ""}
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    rol: state.user.rol,
  };
};

export default connect(mapStateToProps, null)(Layout);

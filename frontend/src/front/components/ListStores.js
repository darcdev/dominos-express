/* eslint-disable jsx-a11y/no-onchange */
import React, { useState, useEffect } from "react";

const ProductsByStore = function ProductsByStore() {
  const [activeCategory, setActiveCategory] = useState("");
  const [categoriesStore, setCategoriesStore] = useState([]);
  const [products, setproducts] = useState([]);

  useEffect(() => {
    async function getStoreCategories() {
      const categories = ["supermercados", "tiendas", "restaurantes"];
      setCategoriesStore(categories);
      setActiveCategory(categories[0]);
    }

    getStoreCategories();
  }, []);
  useEffect(() => {
    async function getProducts() {
      const productsByCategory = [
        {
          logo:
            "https://images.rappi.pe/store_type/market-1598710455.png?d=200x200&e=webp",
          name: "Mercadonna",
          storeId: "594949",
        },
      ];

      console.log(activeCategory);

      setproducts(productsByCategory);
    }

    getProducts();
  }, [activeCategory]);

  const handleChangeCategory = (event) => {
    setActiveCategory(event.target.value);
  };
  return (
    <>
      <div className="row">
        <div className="col-6">
          <div className="input-group mb-3">
            <div className="input-group-prepend">
              <label className="input-group-text" htmlFor="inputGroupSelect01">
                Categoria
              </label>
            </div>
            <select
              className="custom-select"
              id="categoryStore"
              name="categoryStore"
              style={{ width: "auto" }}
              onChange={handleChangeCategory}
            >
              {categoriesStore.map((value, index) => {
                return (
                  <option key={index} value={value}>
                    {value}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
      </div>
      <div className="list-products-store row m-4">
        {products.map((value, index) => {
          return (
            <div className="col-3 mb-4" key={index}>
              <div className="card border-0 rounded">
                <div>
                  <img src={value.logo} className="card-img-top" alt="..." />
                  <div className="card-body pt-2 p-0">
                    <h6 className="text-dark mt-0 text-center">{value.name}</h6>
                    <p className="text-small text-center">
                      Id Tienda : #{value.storeId}
                    </p>
                    <br />
                  </div>
                </div>

                <hr />
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};
export default ProductsByStore;

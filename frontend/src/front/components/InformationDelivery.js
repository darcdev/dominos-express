import React, { useState, useEffect } from "react";

const InformationDelivery = function InformationDelivery() {
  const [deliveryData, setDeliveryData] = useState({
    name: "",
    lastname: "",
    email: "",
    password: "",
    validation: false,
  });

  useEffect(() => {
    async function getDeliveryData() {
      const data = {
        name: "andres",
        lastname: "garcia",
        email: "merca@gmail.com",
        password: "password",
      };
      setDeliveryData(data);
    }
    getDeliveryData();
  }, []);

  const handleValidation = () => {
    const { name, lastname, email, password } = deliveryData;
    if (!(name && lastname && email && password)) {
      setDeliveryData({
        ...deliveryData,
        validation: true,
      });
    } else {
      setDeliveryData({
        ...deliveryData,
        validation: false,
      });
    }
  };
  const handleInput = (event) => {
    setDeliveryData({
      ...deliveryData,
      [event.target.name]: event.target.value,
    });
  };
  const handleUpdateDeliveryData = (event) => {
    event.preventDefault();
    handleValidation();
  };

  return (
    <>
      <div className="row">
        <div className="col-7">
          <h3 className="mb-4 ">Datos del Repartidor</h3>
          <form>
            <div className="form-row">
              <div className="form-group col-md-12">
                <label htmlFor="name">Nombre</label>
                <input
                  type="text"
                  className="form-control"
                  name="name"
                  id="name"
                  placeholder="Nombre"
                  value={deliveryData.name}
                  onChange={handleInput}
                />
              </div>
              <div className="form-group col-md-12">
                <label htmlFor="lastname">Apellido</label>
                <input
                  type="text"
                  className="form-control"
                  id="lastname"
                  name="lastname"
                  placeholder="Apellido"
                  value={deliveryData.lastname}
                  onChange={handleInput}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-12">
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  name="email"
                  placeholder="Email"
                  value={deliveryData.email}
                  onChange={handleInput}
                />
              </div>
            </div>

            <div className="form-row">
              <div className="form-group col-md-12">
                <label htmlFor="password">Contraseña</label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  name="password"
                  placeholder="Contraseña"
                  value={deliveryData.password}
                  onChange={handleInput}
                />
              </div>
            </div>

            {deliveryData.validation ? (
              <div className="mb-4 mb-3" style={{ color: "red" }}>
                Por favor rellene todos los campos
              </div>
            ) : (
              ""
            )}
            <button
              type="submit"
              onClick={handleUpdateDeliveryData}
              className="btn btn-success mr-3"
            >
              Actualizar
            </button>
            <button type="submit" className="btn btn-danger">
              Renunciar
            </button>
          </form>
        </div>
      </div>
    </>
  );
};

export default InformationDelivery;

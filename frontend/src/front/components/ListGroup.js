/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/interactive-supports-focus */
import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { logoutRequest } from "../actions";
import "../assets/styles/components/ListGroup.css";

const ListGroup = function ListGroup({ location }) {
  const action = location;

  const logoutHandler = () => {
    logoutRequest("home");
    window.location.href = "/loguear?usuario=tienda";
  };
  return (
    <div className="list-group " id="list-tab" role="tablist">
      <Link
        to="/tienda/2?action=pedidos-en-curso"
        className={
          "list-group-item  " +
          (action == "pedidos-en-curso" ? "active-section " : "")
        }
        id="pedidos-en-curso"
        data-toggle="list"
        role="tab"
        aria-controls="home"
      >
        Pedidos en Curso
      </Link>
      <Link
        to="/tienda/2?action=listar-productos"
        className={
          "list-group-item " +
          (action == "listar-productos" ? "active-section " : "")
        }
        id="listar-productos"
        data-toggle="list"
        role="tab"
        aria-controls="profile"
      >
        Productos
      </Link>
      <Link
        to="/tienda/2?action=agregar-productos"
        className={
          "list-group-item " +
          (action == "agregar-productos" ? "active-section " : "")
        }
        id="agregar-productos"
        data-toggle="list"
        role="tab"
        aria-controls="profile"
      >
        Agregar Productos
      </Link>
      <Link
        to="/tienda/2?action=historial-de-pedidos"
        className={
          "list-group-item  " +
          (action == "historial-de-pedidos" ? "active-section " : "")
        }
        id="historial-pedidos"
        data-toggle="list"
        role="tab"
        aria-controls="messages"
      >
        Historial de Pedidos
      </Link>
      <Link
        to="/tienda/2?action=informacion-tienda"
        className={
          "list-group-item  " +
          (action == "informacion-tienda" ? "active-section " : "")
        }
        id="informacion-tienda"
        data-toggle="list"
        role="tab"
        aria-controls="settings"
      >
        Información Tienda
      </Link>
      <div
        onClick={logoutHandler}
        style={{ cursor: "pointer" }}
        className={"list-group-item bg-danger text-white "}
        id="informacion-usuario"
        data-toggle="list"
        role="tab"
      >
        Cerrar Sesión
      </div>
    </div>
  );
};

const mapDispatchToProps = {
  logoutRequest,
};

export default connect(null, mapDispatchToProps)(ListGroup);

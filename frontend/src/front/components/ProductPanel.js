import React from "react";
import ProductDetails from "./ProductDetails";
const ProductPanel = function ProductPanel({
  activeProduct,
  handleClose,
  addProduct,
}) {
  return (
    <>
      {activeProduct ? (
        <div className="row">
          <div className="col-12">
            <ProductDetails
              show={activeProduct}
              handleClose={handleClose}
              addProduct={addProduct}
            />
          </div>
        </div>
      ) : (
        ""
      )}
    </>
  );
};

export default ProductPanel;

import React from "react";
import { Link } from "react-router-dom";
import "../assets/styles/components/Footer.css";

const Footer = function Footer() {
  return (
    <footer
      className="page-footer font-small mdb-color pt-4"
      style={{ backgroundColor: "#3e4551", color: "white" }}
    >
      <div className="container text-center text-md-left">
        <div className="row text-center text-md-left mt-3 pb-3">
          <div className="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
            <h6 className="text-uppercase mb-4 font-weight-bold">
              DominosExpress
            </h6>
            <p style={{ color: "white" }}>
              Llevamos su pedido hasta la puerta de su casa
            </p>
          </div>

          <hr className="w-100 clearfix d-md-none" />

          <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
            <h6 className="text-uppercase mb-4 font-weight-bold">Categorias</h6>
            <p>
              <Link className="color-link-footer" to="/">
                Supermercados
              </Link>
            </p>
            <p>
              <Link className="color-link-footer" to="/">
                Restaurantes
              </Link>
            </p>
            <p>
              <Link className="color-link-footer" to="/">
                Farmacias
              </Link>
            </p>
            <p>
              <Link className="color-link-footer" to="/">
                Bebidas y Licores
              </Link>
            </p>
          </div>

          <hr className="w-100 clearfix d-md-none" />

          <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
            <h6 className="text-uppercase mb-4 font-weight-bold">De Interes</h6>
            <p>
              <Link className="color-link-footer" to="/loguear?usuario=tienda">
                DomiTienda
              </Link>
            </p>
            <p>
              <Link
                className="color-link-footer"
                to="/loguear?usuario=repartidor"
              >
                DomiTendero
              </Link>
            </p>
            <p>
              <Link
                className="color-link-footer"
                to="/registrar?usuario=tienda"
              >
                Registrar Tienda
              </Link>
            </p>
            <p>
              <Link
                className="color-link-footer"
                to="/registrar?usuario=repartidor"
              >
                Trabaja con Nosotros
              </Link>
            </p>
          </div>

          <hr className="w-100 clearfix d-md-none" />

          <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
            <h6 className="text-uppercase mb-4 font-weight-bold">Contacto</h6>
            <p style={{ color: "white" }}>
              <i className="fas fa-home mr-3"></i> Villavicencio , Meta
            </p>
            <p style={{ color: "white" }}>
              <i className="fas fa-envelope mr-3"></i> dominosExpress@gmail.com
            </p>
            <p style={{ color: "white" }}>
              <i className="fas fa-phone mr-3"></i> +57 3045215421
            </p>
            <p style={{ color: "white" }}>
              <i className="fas fa-print mr-3"></i> +57 3102015689
            </p>
          </div>
        </div>

        <hr />

        <div className="row d-flex align-items-center">
          <div className="col-md-7 col-lg-8">
            <p className="text-center text-md-left" style={{ color: "white" }}>
              © 2020 Todos los Derechos Reservados
            </p>
          </div>

          <div className="col-md-5 col-lg-4 ml-lg-0">
            <div className="text-center text-md-right">
              <ul className="list-unstyled list-inline">
                <li className="list-inline-item">
                  <a className="btn-floating btn-sm rgba-white-slight mx-1">
                    <i className="fab fa-facebook-f"></i>
                  </a>
                </li>
                <li className="list-inline-item">
                  <a className="btn-floating btn-sm rgba-white-slight mx-1">
                    <i className="fab fa-twitter"></i>
                  </a>
                </li>
                <li className="list-inline-item">
                  <a className="btn-floating btn-sm rgba-white-slight mx-1">
                    <i className="fab fa-google-plus-g"></i>
                  </a>
                </li>
                <li className="list-inline-item">
                  <a className="btn-floating btn-sm rgba-white-slight mx-1">
                    <i className="fab fa-linkedin-in"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
};

export default Footer;

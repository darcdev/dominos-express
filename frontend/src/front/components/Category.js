import React from "react";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import { LinkContainer } from "react-router-bootstrap";
const customStyles = {
  overlay: { zIndex: 2000 },
};
const Category = function Category({ show, handleClose }) {
  return (
    <Modal style={customStyles} size="lg" show={show} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Ver Más Categorias</Modal.Title>
        <hr />
      </Modal.Header>
      <Modal.Body>
        <Container>
          <Row>
            <Col md={4}>
              <LinkContainer
                to="/tiendas/tipo/hogar"
                className="category-over-effect"
              >
                <div
                  className="card border-0 rounded"
                  style={{ width: "12rem" }}
                >
                  <img
                    src="https://images.rappi.cl/store_type/hogar-1601559447.png?d=200x200&e=webp"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body">
                    <h6 className="card-title text-center text-dark">Hogar</h6>
                  </div>
                </div>
              </LinkContainer>
            </Col>
            <Col md={4}>
              <LinkContainer
                to="/tiendas/tipo/tecnología"
                className="category-over-effect"
              >
                <div
                  className="card border-0 rounded"
                  style={{ width: "12rem" }}
                >
                  <img
                    src="https://images.rappi.cl/store_type/technology-1601559741.png?d=200x200&e=webp"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body">
                    <h6 className="card-title text-center text-dark">
                      Tecnología
                    </h6>
                  </div>
                </div>
              </LinkContainer>
            </Col>
            <Col md={4}>
              <LinkContainer
                to="/tiendas/tipo/mascotas"
                className="category-over-effect"
              >
                <div
                  className="card border-0 rounded"
                  style={{ width: "12rem" }}
                >
                  <img
                    src="https://images.rappi.cl/store_type/mascotas-1601559858.png?d=200x200&e=webp"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body">
                    <h6 className="card-title text-center text-dark">
                      Mascotas
                    </h6>
                  </div>
                </div>
              </LinkContainer>
            </Col>
            <Col md={4}>
              <LinkContainer
                to="/tiendas/tipo/libreria"
                className="category-over-effect"
              >
                <div
                  className="card border-0 rounded"
                  style={{ width: "12rem" }}
                >
                  <img
                    src="https://images.rappi.cl/store_type/libreria-1601559883.png?d=200x200&e=webp"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body">
                    <h6 className="card-title text-center text-dark">
                      Librería
                    </h6>
                  </div>
                </div>
              </LinkContainer>
            </Col>
            <Col md={4}>
              <LinkContainer
                to="/tiendas/tipo/jugueteria"
                className="category-over-effect"
              >
                <div
                  className="card border-0 rounded"
                  style={{ width: "12rem" }}
                >
                  <img
                    src="https://images.rappi.cl/store_type/jugueteria-1601559838.png?d=200x200&e=webp"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body">
                    <h6 className="card-title text-center text-dark">
                      Jugueteria
                    </h6>
                  </div>
                </div>
              </LinkContainer>
            </Col>
            <Col md={4}>
              <LinkContainer
                to="/tiendas/tipo/deportes"
                className="category-over-effect"
              >
                <div
                  className="card border-0 rounded"
                  style={{ width: "12rem" }}
                >
                  <img
                    src="https://images.rappi.cl/store_type/sports-1601559783.png?d=200x200&e=webp"
                    className="card-img-top"
                    alt="..."
                  />
                  <div className="card-body">
                    <h6 className="card-title text-center text-dark">
                      Deportes
                    </h6>
                  </div>
                </div>
              </LinkContainer>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
    </Modal>
  );
};

export default Category;

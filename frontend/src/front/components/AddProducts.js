/* eslint-disable jsx-a11y/no-onchange */
import React, { useEffect, useState } from "react";

const AddProducts = function () {
  const [dataStore, setDataStore] = useState({
    logo: "",
    name: "",
    description: "",
    storeCategory: "",
    price: "",
    stock: "",
    validation: false,
  });
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    async function getCategories() {
      const data = ["supermercados", "relojeria", "restaurantes"];
      setCategories(data);
    }
    getCategories();
  }, []);

  const handleInput = (event) => {
    setDataStore({
      ...dataStore,
      [event.target.name]: event.target.value,
    });
  };

  const handleValidation = () => {
    const { logo, name, price, description, storeCategory, stock } = dataStore;
    if (!(logo && name && price && description && storeCategory && stock)) {
      setDataStore({
        ...dataStore,
        validation: true,
      });
    } else {
      setDataStore({
        ...dataStore,
        validation: false,
      });
    }
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    handleValidation();
  };
  return (
    <>
      <div className="row">
        <div className="col-7">
          <h3 className="mb-4 ">Datos del Producto</h3>
          <form>
            <div className="form-row mb-3 text-center">
              <div className="form-group">
                <img
                  src="https://images.rappi.com/marketplace/hiper-1601184974.png?d=200x200&e=webp"
                  className="rounded-lg mb-3"
                  alt="..."
                />

                <div className="custom-file text-left" style={{ width: "80%" }}>
                  <input
                    type="file"
                    className="custom-file-input"
                    lang="es"
                    id="logo"
                    name="logo"
                    onChange={handleInput}
                  />
                  <label className="custom-file-label" htmlFor="logo">
                    Seleccionar Logo
                  </label>
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-12">
                <label htmlFor="name">Nombre</label>
                <input
                  type="text"
                  className="form-control"
                  placeholder="Nombre"
                  id="name"
                  name="name"
                  value={dataStore.name}
                  onChange={handleInput}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-12">
                <label htmlFor="description">Descripción</label>

                <div className="form-group">
                  <textarea
                    className="form-control"
                    placeholder="Descripción del producto"
                    rows="4"
                    id="description"
                    name="description"
                    value={dataStore.description}
                    onChange={handleInput}
                  ></textarea>
                </div>
              </div>
            </div>
            <div className="row mb-2">
              <div className="col-12">
                <div className="input-group mb-3">
                  <div className="input-group-prepend">
                    <label className="input-group-text" htmlFor="category">
                      Categoria
                    </label>
                  </div>
                  <select
                    className="custom-select"
                    id="category"
                    name="storeCategory"
                    value={dataStore.storeCategory}
                    onChange={handleInput}
                    style={{ width: "auto" }}
                  >
                    {categories.map((value, index) => {
                      return (
                        <option key={index} value={value}>
                          {value}
                        </option>
                      );
                    })}
                  </select>
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-6">
                <label htmlFor="price">Precio</label>
                <input
                  type="number"
                  className="form-control"
                  id="price"
                  name="price"
                  onChange={handleInput}
                  value={dataStore.price}
                  placeholder="Precio"
                />
              </div>
              <div className="form-group col-md-6">
                <label htmlFor="stock">Stock</label>
                <input
                  type="number"
                  className="form-control"
                  id="stock"
                  name="stock"
                  onChange={handleInput}
                  value={dataStore.stock}
                  placeholder="stock"
                />
              </div>
            </div>
            {dataStore.validation ? (
              <div className="mb-4 mb-3" style={{ color: "red" }}>
                Por favor rellene todos los campos
              </div>
            ) : (
              ""
            )}

            <button
              type="submit"
              onClick={handleSubmit}
              className="btn btn-success"
            >
              Agregar
            </button>
          </form>
        </div>
      </div>
    </>
  );
};

export default AddProducts;

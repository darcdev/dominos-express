/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/interactive-supports-focus */
import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { logoutRequest } from "../actions";
import "../assets/styles/components/ListGroup.css";

const ListGroupUser = function ListGroupUser({ location }) {
  const action = location;

  const logoutHandler = () => {
    logoutRequest("home");
    window.location.href = "/loguear?usuario=cliente";
  };
  return (
    <div className="list-group " id="list-tab" role="tablist">
      <Link
        to="/usuario/2?action=mis-pedidos"
        className={
          "list-group-item  " +
          (action == "mis-pedidos" ? "active-section " : "")
        }
        id="pedidos-en-curso"
        data-toggle="list"
        role="tab"
        aria-controls="home"
      >
        Mis Pedidos En curso
      </Link>

      <Link
        to="/usuario/2?action=historial-de-pedidos"
        className={
          "list-group-item  " +
          (action == "historial-de-pedidos" ? "active-section " : "")
        }
        id="historial-pedidos"
        data-toggle="list"
        role="tab"
        aria-controls="messages"
      >
        Ordenes Anteriores
      </Link>
      <Link
        to="/usuario/2?action=informacion-usuario"
        className={
          "list-group-item  " +
          (action == "informacion-usuario" ? "active-section " : "")
        }
        id="informacion-usuario"
        data-toggle="list"
        role="tab"
        aria-controls="settings"
      >
        Información usuario
      </Link>

      <div
        onClick={logoutHandler}
        style={{ cursor: "pointer" }}
        className={"list-group-item bg-danger text-white "}
        id="informacion-usuario"
        data-toggle="list"
        role="tab"
        aria-controls="settings"
      >
        Cerrar Sesión
      </div>
    </div>
  );
};
const mapDispatchToProps = {
  logoutRequest,
};

export default connect(null, mapDispatchToProps)(ListGroupUser);

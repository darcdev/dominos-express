import React, { useState, useEffect } from "react";

const InformationUser = function InformationUser() {
  const [userData, setUserData] = useState({
    validation: false,
  });

  useEffect(() => {
    async function getUserData() {
      const data = {
        name: "andres",
        lastname: "garcia",
        address: "av 6 13",
        email: "merca@gmail.com",
        ident: 145040,
        phone: 304521540,
        password: "password",
      };
      setUserData(data);
    }
    getUserData();
  }, []);

  const handleValidation = () => {
    const { name, lastname, email, address, ident, phone, password } = userData;
    if (!(name && lastname && email && address && password && ident && phone)) {
      setUserData({
        ...userData,
        validation: true,
      });
    } else {
      setUserData({
        ...userData,
        validation: false,
      });
    }
  };
  const handleInput = (event) => {
    setUserData({
      ...userData,
      [event.target.name]: event.target.value,
    });
  };
  const handleUpdateUserData = (event) => {
    event.preventDefault();
    handleValidation();
  };

  return (
    <>
      <div className="row">
        <div className="col-7">
          <h3 className="mb-4 ">Datos del Usuario</h3>
          <form>
            <div className="form-row">
              <div className="form-group col-md-10">
                <label htmlFor="name">Nombre</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  name="name"
                  value={userData.name}
                  placeholder="Nombre"
                  onChange={handleInput}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-10">
                <label htmlFor="lastname">Apellido</label>
                <input
                  type="text"
                  className="form-control"
                  id="lastname"
                  name="lastname"
                  value={userData.lastname}
                  placeholder="Apellido"
                  onChange={handleInput}
                />
              </div>
            </div>

            <div className="form-row">
              <div className="form-group col-md-10">
                <label htmlFor="address">Dirección</label>
                <input
                  type="text"
                  className="form-control"
                  id="address"
                  name="address"
                  placeholder="1234 Calle 6"
                  onChange={handleInput}
                />
              </div>
            </div>

            <div className="form-row">
              <div className="form-group col-md-10">
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  name="email"
                  value={userData.email}
                  placeholder="Email"
                  onChange={handleInput}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-10">
                <label htmlFor="ident">Cedula</label>
                <input
                  type="number"
                  className="form-control"
                  id="ident"
                  name="ident"
                  value={userData.ident}
                  placeholder="Cedula"
                  onChange={handleInput}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-10">
                <label htmlFor="phone">Telefono</label>
                <input
                  type="number"
                  className="form-control"
                  id="phone"
                  name="phone"
                  value={userData.phone}
                  placeholder="Telefono"
                  onChange={handleInput}
                />
              </div>
            </div>

            <div className="form-row">
              <div className="form-group col-md-10">
                <label htmlFor="password">Contraseña</label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  name="password"
                  value={userData.password}
                  placeholder="Contraseña"
                  onChange={handleInput}
                />
              </div>
            </div>

            {userData.validation ? (
              <div className="mb-4 mb-3" style={{ color: "red" }}>
                Por favor rellene todos los campos
              </div>
            ) : (
              ""
            )}
            <button
              type="submit"
              onClick={handleUpdateUserData}
              className="btn btn-success"
            >
              Actualizar
            </button>
          </form>
        </div>
      </div>
    </>
  );
};

export default InformationUser;

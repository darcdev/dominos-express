/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/interactive-supports-focus */
import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { logoutRequest } from "../actions";

import "../assets/styles/components/ListGroup.css";

const ListGroupAdmin = function ListGroupAdmin({ location }) {
  const action = location;
  const logoutHandler = () => {
    logoutRequest("home");
    window.location.href = "/loguear?usuario=admin";
  };
  return (
    <div className="list-group " id="list-tab" role="tablist">
      <Link
        to="/admin/2?action=pedidos-en-curso"
        className={
          "list-group-item  " +
          (action == "pedidos-en-curso" ? "active-section " : "")
        }
        id="pedidos-en-curso"
        data-toggle="list"
        role="tab"
        aria-controls="home"
      >
        Pedidos en Curso
      </Link>
      <Link
        to="/admin/2?action=listar-tiendas"
        className={
          "list-group-item " +
          (action == "listar-tiendas" ? "active-section " : "")
        }
        id="listar-tiendas"
        data-toggle="list"
        role="tab"
        aria-controls="profile"
      >
        Tiendas
      </Link>
      <Link
        to="/admin/2?action=listar-repartidores"
        className={
          "list-group-item " +
          (action == "listar-repartidores" ? "active-section " : "")
        }
        id="listar-repartidores"
        data-toggle="list"
        role="tab"
        aria-controls="profile"
      >
        Repartidores
      </Link>
      <Link
        to="/admin/2?action=historial-de-pedidos"
        className={
          "list-group-item  " +
          (action == "historial-de-pedidos" ? "active-section " : "")
        }
        id="historial-pedidos"
        data-toggle="list"
        role="tab"
        aria-controls="messages"
      >
        Historial de Pedidos
      </Link>
      <Link
        to="/admin/2?action=categorias"
        className={
          "list-group-item  " +
          (action == "categorias" ? "active-section " : "")
        }
        id="historial-pedidos"
        data-toggle="list"
        role="tab"
        aria-controls="messages"
      >
        Categorias
      </Link>
      <Link
        to="/admin/2?action=informacion-admin"
        className={
          "list-group-item  " +
          (action == "informacion-admin" ? "active-section " : "")
        }
        id="informacion-admin"
        data-toggle="list"
        role="tab"
        aria-controls="settings"
      >
        Información Administrativa
      </Link>
      <div
        onClick={logoutHandler}
        style={{ cursor: "pointer" }}
        className={"list-group-item bg-danger text-white "}
        id="informacion-usuario"
        data-toggle="list"
        role="tab"
        aria-controls="settings"
      >
        Cerrar Sesión
      </div>
    </div>
  );
};

const mapDispatchToProps = {
  logoutRequest,
};
export default connect(null, mapDispatchToProps)(ListGroupAdmin);

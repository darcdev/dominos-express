/* eslint-disable jsx-a11y/no-onchange */
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "../assets/styles/components/Login.css";
const RegisterStore = function RegisterStore() {
  const [form, setForm] = useState({
    username: "",
    category: "",
    email: "",
    phone: "",
    address: "",
    password: "",
    validation: false,
  });

  const [categories, setCategories] = useState([]);

  useEffect(() => {
    async function getCategories() {
      const categories = [
        "supermercado",
        "tienda",
        "accesorios",
        "restaurantes",
      ];

      setCategories(categories);
    }
    getCategories();
  }, []);

  const handleInput = (event) => {
    setForm({
      ...form,
      [event.target.name]: event.target.value,
    });
  };
  const handleValidation = () => {
    const { username, category, email, phone, address, password } = form;
    if (!(username && category && email && phone && address && password)) {
      setForm({
        ...form,
        validation: true,
      });
    } else {
      setForm({
        ...form,
        validation: false,
      });
    }
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    handleValidation();
  };
  return (
    <div className="limiter">
      <div className="container-login100">
        <div className="wrap-login100">
          <div className="login100-form-title">
            <span className="login100-form-title-1">Registrar Tienda</span>
          </div>

          <form className="login100-form validate-form">
            <div
              className="mb-4 wrap-input100 validate-input m-b-26"
              data-validate="nombre es requerido"
            >
              <span className="label-input100">Nombre Tienda</span>
              <input
                className="input100"
                type="text"
                name="username"
                value={form.username}
                placeholder="Digite nombre de la tienda"
                onChange={handleInput}
              />
              <span className="focus-input100"></span>
            </div>
            <div
              className="mb-4 wrap-input100 validate-input m-b-26"
              data-validate="apellido es requerido"
            >
              <span className="label-input100">Categoria Tienda</span>
              <select
                name="category"
                className="custom-select"
                id="inputGroupSelect01"
                value={form.category}
                onChange={handleInput}
              >
                {categories.map((value, index) => {
                  return (
                    <option key={index} value={value}>
                      {value}
                    </option>
                  );
                })}
              </select>
              <span className="focus-input100"></span>
            </div>
            <div
              className="mb-4 wrap-input100 validate-input m-b-26"
              data-validate="Correo es requerido"
            >
              <span className="label-input100">Correo</span>
              <input
                className="input100"
                type="email"
                name="email"
                value={form.email}
                placeholder="Digite el correo asociado"
                onChange={handleInput}
              />
              <span className="focus-input100"></span>
            </div>
            <div
              className="mb-4 wrap-input100 validate-input m-b-26"
              data-validate="telefono es requerido"
            >
              <span className="label-input100">Telefono</span>
              <input
                className="input100"
                type="text"
                name="phone"
                value={form.phone}
                onChange={handleInput}
                placeholder="Digite el telefono asociado"
              />
              <span className="focus-input100"></span>
            </div>
            <div
              className="mb-4 wrap-input100 validate-input m-b-26"
              data-validate="telefono es requerido"
            >
              <span className="label-input100">Direccion Principal</span>
              <input
                className="input100"
                type="text"
                name="address"
                value={form.address}
                placeholder="Digite la direccion de la tienda"
                onChange={handleInput}
              />
              <span className="focus-input100"></span>
            </div>
            <div
              className="mb-4 wrap-input100 validate-input m-b-18"
              data-validate="contraseña is requerida"
            >
              <span className="label-input100">Contraseña</span>
              <input
                className="input100"
                type="password"
                name="password"
                value={form.password}
                onChange={handleInput}
                placeholder="Digite la contraseña"
              />
              <span className="focus-input100"></span>
            </div>

            {form.validation ? (
              <div className="mb-4 mb-3" style={{ color: "red" }}>
                Por favor rellene todos los campos
              </div>
            ) : (
              ""
            )}
            <div className="mb-4 container-login100-form-btn">
              <button className="login100-form-btn" onClick={handleSubmit}>
                Registrar Tienda
              </button>
            </div>

            <div>
              <small>Ya tienes una tienda ? </small>
              <Link to="/loguear?usuario=tienda">Iniciar Sesión</Link>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default RegisterStore;

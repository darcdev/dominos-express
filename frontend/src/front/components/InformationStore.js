import React, { useState, useEffect } from "react";

const InformationStore = function () {
  const [storeData, setStoreData] = useState({
    validation: false,
  });

  useEffect(() => {
    async function getStoreData() {
      const data = {
        name: "merca",
        logo:
          "https://images.rappi.com/marketplace/hiper-1601184974.png?d=200x200&e=webp",
        email: "merca@gmail.com",
        password: "password",
        address: "av 6 calle 8",
      };
      setStoreData({ ...storeData, ...data });
    }
    getStoreData();
  }, []);

  const handleValidation = () => {
    const { name, email, password, address } = storeData;
    if (!(name && email && password && address)) {
      setStoreData({
        ...storeData,
        validation: true,
      });
    } else {
      setStoreData({
        ...storeData,
        validation: false,
      });
    }
  };
  const handleInput = (event) => {
    setStoreData({
      ...storeData,
      [event.target.name]: event.target.value,
    });
  };
  const handleUpdateStoreData = (event) => {
    event.preventDefault();
    handleValidation();
  };
  return (
    <>
      <div className="row">
        <div className="col-7">
          <h3 className="mb-4 ">Datos de la Tienda</h3>
          <form>
            <div className="form-row mb-3 text-center">
              <div className="form-group">
                <img
                  src={storeData.logo}
                  className="rounded-lg mb-3"
                  alt="..."
                  value={storeData.logo}
                />

                <div
                  className="custom-file text-left"
                  style={{ width: "80%", color: "black" }}
                >
                  <input
                    style={{ color: "black" }}
                    type="file"
                    className="custom-file-input"
                    id="customFileLang"
                    name="logo"
                    onChange={handleInput}
                  />
                  <label className="custom-file-label" htmlFor="customFileLang">
                    Seleccionar Logo
                  </label>
                </div>
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-12">
                <label htmlFor="name">Nombre Tienda</label>
                <input
                  type="text"
                  className="form-control"
                  id="name"
                  name="name"
                  placeholder="Nombre"
                  value={storeData.name}
                  onChange={handleInput}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-12">
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  className="form-control"
                  id="email"
                  name="email"
                  placeholder="Email"
                  value={storeData.email}
                  onChange={handleInput}
                />
              </div>
              <div className="form-group col-md-12">
                <label htmlFor="password">Contraseña</label>
                <input
                  type="password"
                  className="form-control"
                  id="password"
                  name="password"
                  placeholder="Contraseña"
                  value={storeData.password}
                  onChange={handleInput}
                />
              </div>
            </div>
            <div className="form-group">
              <label htmlFor="address">Dirección</label>
              <input
                type="text"
                className="form-control"
                id="address"
                name="address"
                placeholder="direccion"
                value={storeData.address}
                onChange={handleInput}
              />
            </div>
            {storeData.validation ? (
              <div className="mb-4 mb-3" style={{ color: "red" }}>
                Por favor rellene todos los campos
              </div>
            ) : (
              ""
            )}

            <button
              onClick={handleUpdateStoreData}
              type="submit"
              className="btn btn-success"
            >
              Actualizar
            </button>
          </form>
        </div>
      </div>
    </>
  );
};

export default InformationStore;

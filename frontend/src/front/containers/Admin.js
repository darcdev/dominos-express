import React from "react";
import Categories from "../components/Categories";
import ListDeliveries from "../components/ListDeliveries";
import ListGroupAdmin from "../components/ListGroupAdmin";
import ListStore from "../components/ListStores";
import Orders from "../components/Orders";
import InformationAdmin from "../components/InformationAdmin";

const AdminPanel = function AdminPanel({ location }) {
  const action = location.search.split("=")[1];

  return (
    <div className="container-fluid mt-4">
      <div className="row">
        <div className="col-12">
          <h2 className="mb-3">
            Administrador DominosExpress{" "}
            <span
              style={{ fontSize: "0.98rem" }}
              className="badge badge-secondary"
            >
              Panel Administrativo
            </span>
          </h2>
          <hr />
        </div>
      </div>
      <div className="row mt-5 mb-5">
        <div className="col-4">
          <ListGroupAdmin location={action} />
        </div>

        <div className="col-8 position">
          <div className="tab-content" id="nav-tabContent col-12">
            <div
              className="tab-pane fade show active"
              id="list-home"
              role="tabpanel"
              aria-labelledby="list-home-list"
            ></div>
            {action == "pedidos-en-curso" ? <Orders user="admin" /> : ""}
            {action == "listar-tiendas" ? <ListStore /> : ""}
            {action == "listar-repartidores" ? <ListDeliveries /> : ""}
            {action == "historial-de-pedidos" ? <Orders /> : ""}
            {action == "categorias" ? <Categories /> : ""}
            {action == "informacion-admin" ? <InformationAdmin /> : ""}
          </div>
        </div>
      </div>
    </div>
  );
};

export default AdminPanel;

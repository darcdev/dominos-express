/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useState } from "react";
import { connect } from "react-redux";
import Carrousel from "../components/Carousel";
import "../assets/styles/containers/Home.css";
import { Link } from "react-router-dom";
import CategoryPanel from "../components/CategoryPanel";

const Home = function Home() {
  const [activePanel, setActivePanel] = useState(false);

  const toggleCategoryPanel = () => {
    setActivePanel(!activePanel);
  };
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-12 m-auto">
          <Carrousel />
        </div>
      </div>
      <div className="row mt-5">
        <div className="col-12">
          <p className="text-muted text-uppercase">Ordena lo que desees</p>
          <h2>Nuestras Tiendas</h2>
        </div>
      </div>

      <div className="row mt-5 justify-content-center ">
        <div className="col-2 ">
          <div className="card border-0 rounded" style={{ width: "12rem" }}>
            <Link to="/tiendas/tipo/supermercados">
              <img
                src="https://images.rappi.pe/store_type/market-1598710455.png?d=200x200&e=webp"
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title text-center text-dark">
                  Supermercados
                </h6>
              </div>
            </Link>
          </div>
        </div>
        <div className="col-2 ">
          <div className="card border-0" style={{ width: "12rem" }}>
            <Link to="/tiendas/tipo/restaurantes">
              <img
                src="https://images.rappi.pe/store_type/restaurant-1597276359.png?d=200x200&e=webp"
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title text-center text-dark">
                  Restaurantes
                </h6>
              </div>
            </Link>
          </div>
        </div>
        <div className="col-2 ">
          <div className="card border-0" style={{ width: "12rem" }}>
            <Link to="/tiendas/tipo/farmacias">
              <img
                src="https://images.rappi.pe/store_type/farmacia-1598710542.png?d=200x200&e=webp"
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title text-center text-dark">Farmacias</h6>
              </div>
            </Link>
          </div>
        </div>
        <div className="col-2 ">
          <div className="card border-0" style={{ width: "12rem" }}>
            <Link to="/tiendas/tipo/bebidas y licores">
              <img
                src="https://images.rappi.pe/store_type/licores-1598731525.png?d=200x200&e=webp"
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title text-center text-dark">
                  Bebidas y Licores
                </h6>
              </div>
            </Link>
          </div>
        </div>
        <div className="col-2 " onClick={toggleCategoryPanel}>
          <div className="card border-0" style={{ width: "12rem" }}>
            <Link to="">
              <img
                src="https://cdn.onlinewebfonts.com/svg/img_85968.png"
                className="card-img-top"
                alt="..."
              />
              <div className="card-body">
                <h6 className="card-title text-center text-dark">Ver Más</h6>
              </div>
            </Link>
          </div>
        </div>
      </div>
      <div className="row mt-5">
        <div className="col-12">
          <CategoryPanel
            activePanel={activePanel}
            handleClose={toggleCategoryPanel}
          />
        </div>
      </div>

      <div className="row mt-5">
        <div className="col-12">
          <div className="jumbotron">
            <h1 className="display-4">Disfruta de un mundo de beneficios </h1>
            <p className="lead">
              Te ofrecemos los productos de mas alta calidad con los mejores
              precios
            </p>
            <hr className="my-4" />
            <h6>
              No lo pienses mas , elige lo que desees y te lo llevamos hasta la
              puerta de tu casa
            </h6>
          </div>
        </div>
      </div>
      <hr className="mt-5" />

      <div className="row mb-5 justify-content-center">
        <div className="col-3">
          <div className="card border-0 text-center" style={{ width: "18rem" }}>
            <div className="card-body">
              <h5 className="card-title">Calidad Garantizada</h5>
              <p className="card-text">
                Te ofrecemos los mejores productos del mercado a los mejores
                precios
              </p>
            </div>
          </div>
        </div>
        <div className="col-3">
          <div className="card border-0 text-center" style={{ width: "18rem" }}>
            <div className="card-body">
              <h5 className="card-title">Ahorra Tiempo</h5>
              <p className="card-text">
                Te llevamos tus productos en menos de lo que esperas hasta la
                puerta de tu casa
              </p>
            </div>
          </div>
        </div>
        <div className="col-3">
          <div
            className="card  border-0 text-center"
            style={{ width: "18rem" }}
          >
            <div className="card-body">
              <h5 className="card-title">Tiendas Reconocidas</h5>
              <p className="card-text">
                Te ofrecemos las mejores Tiendas del mercado para tu
                tranquilidad , ofreciendo variedad de productos
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    user: state.user,
  };
};
export default connect(mapStateToProps, null)(Home);

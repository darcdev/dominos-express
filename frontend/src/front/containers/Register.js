import React from "react";
import RegisterStore from "../components/RegisterStore";
import RegisterRep from "../components/RegisterRep";
import RegisterClient from "../components/RegisterClient";
import { Redirect } from "react-router-dom";

const Register = function Register({ location }) {
  return (
    <div>
      {(() => {
        switch (location.search) {
          case "?usuario=cliente":
            return <RegisterClient />;
          case "?usuario=tienda":
            return <RegisterStore />;
          case "?usuario=repartidor":
            return <RegisterRep />;
          default:
            return <Redirect to="/" />;
        }
      })()}
    </div>
  );
};

export default Register;

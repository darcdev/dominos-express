import React from "react";
import InformationStore from "../components/InformationStore";
import ListGroup from "../components/ListGroup";
import Orders from "../components/Orders";
import ProductsByStore from "../components/ProductsByStore";
import AddProducts from "../components/AddProducts";

const StorePanel = function StorePanel(props) {
  const action = location.search.split("=")[1];
  return (
    <div className="container-fluid mt-4">
      <div className="row">
        <div className="col-12">
          <h2 className="mb-3">
            Tienda Merkat{" "}
            <span
              style={{ fontSize: "0.98rem" }}
              className="badge badge-secondary"
            >
              Panel Administrativo
            </span>
          </h2>
          <hr />
        </div>
      </div>
      <div className="row mt-5 mb-5">
        <div className="col-4">
          <ListGroup location={action} />
        </div>

        <div className="col-8 position">
          <div className="tab-content" id="nav-tabContent col-12">
            <div
              className="tab-pane fade show active"
              id="list-home"
              role="tabpanel"
              aria-labelledby="list-home-list"
            ></div>
            {action == "pedidos-en-curso" ? <Orders /> : ""}
            {action == "listar-productos" ? <ProductsByStore /> : ""}
            {action == "agregar-productos" ? <AddProducts /> : ""}
            {action == "historial-de-pedidos" ? <Orders active="false" /> : ""}
            {action == "informacion-tienda" ? <InformationStore /> : ""}
          </div>
        </div>
      </div>
    </div>
  );
};

export default StorePanel;

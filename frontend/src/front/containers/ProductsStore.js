import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import ProductPanel from "../components/ProductPanel";
import { addProductCart, incrementProduct, decrementProduct } from "../actions";
import { getItem } from "../utils/cart";
import axios from "axios";
import { API_URL } from "../../../config";

const ProductsStore = function ProductsStore({
  incrementProduct,
  decrementProduct,
  match,
  addProductCart,
  cartProducts,
}) {
  const [activeProduct, setActiveProduct] = useState({
    active: false,
    data: "",
  });
  const [storeCategory, setStoreCategory] = useState("");
  const [products, setProducts] = useState([]);
  const [storeCategories, setStoreCategories] = useState([]);

  useEffect(() => {
    async function getCategoryStore() {
      const category = await axios.get(
        `${API_URL}/api/tiendas/categorias/${match.params.name}`
      );
      setStoreCategory(category.data.categoria.nombre_cat);
    }

    async function getProducts() {
      const storeCatego = await axios.get(
        `${API_URL}/api/tiendas/${match.params.name}/productos/categorias`
      );
      const dataProducts = storeCatego.data.categorias.map(
        async (value, index) => {
          const products = await axios.get(
            `${API_URL}/api/tiendas/${match.params.name}/productos/categorias/${value.nombre_cat}`
          );
          return {
            category: value.nombre_cat,
            products: products.data.productos,
          };
        }
      );
      Promise.all(dataProducts).then((value) => {
        console.log(value);
        setProducts(value);
      });
    }
    getCategoryStore();
    getProducts();
  }, []);

  const toggleProductPanel = (value) => {
    setActiveProduct({
      ...activeProduct,
      active: !activeProduct.active,
      data: value ? value : "",
    });
  };

  const addProductToCart = (value) => {
    if (!getItem(value.cod_producto, cartProducts)) {
      value.id = value.cod_producto;
      addProductCart(value);
    }
  };

  const handleIncrement = (id) => {
    incrementProduct(id);
  };
  const handleDecrement = (id) => {
    decrementProduct(id);
  };
  const nameStore = match.params.name;

  return (
    <div className="container-fluid">
      <div className="row mt-4">
        <div className="col-12">
          <h3 className="mb-3">
            {storeCategory.charAt(0).toUpperCase() + storeCategory.slice(1) ||
              " "}{" "}
            &gt; {nameStore.charAt(0).toUpperCase() + nameStore.slice(1)}
          </h3>
        </div>
      </div>
      <div className="row mb-5">
        <div className="col-12">
          <hr />
          <h3 className="ml-3 text-center">Productos por Categoria</h3>
        </div>
      </div>
      <div className="row mt-5">
        <div className="col-12">
          <ProductPanel
            addProduct={activeProduct.data}
            activeProduct={activeProduct.active}
            handleClose={toggleProductPanel}
          />
        </div>
      </div>
      {console.log(products)}
      <div className="row m-auto  " style={{ width: "100%" }}>
        {products.map((value, index) => {
          return (
            <>
              <div key={index} className="mb-3 col-12" key={index}>
                <h4>{value.category}</h4>
                <hr />
              </div>

              {value.products.map((value, index) => {
                return (
                  <div
                    style={{
                      display: "flex",
                    }}
                    className="col-2 mb-5"
                    key={index}
                  >
                    <div className="card border-0 rounded">
                      <Link to="#" onClick={() => toggleProductPanel(value)}>
                        <img
                          src={`https://images.rappi.com/store_type/hogar-1591402720.png?d=200x200&e=webp`}
                          className="card-img-top"
                          alt="..."
                        />{" "}
                        <div className="card-body pt-2 p-0">
                          <b className="text-dark">${value.precio}</b>
                          <br />
                          <p className="text-dark mt-0">
                            {value.nombre_producto.toLowerCase()}
                          </p>
                          <br />
                        </div>
                      </Link>

                      {getItem(value.cod_producto, cartProducts) ? (
                        <div
                          style={{ flex: "1" }}
                          className="cont-info-simulate"
                        >
                          <button
                            onClick={() => handleDecrement(value.cod_producto)}
                            className="mr-4"
                          >
                            {getItem(value.cod_producto, cartProducts).cant ==
                            1 ? (
                              <img
                                style={{ width: "15px" }}
                                src="https://img.icons8.com/android/24/000000/trash.png"
                                alt=""
                              />
                            ) : (
                              "-"
                            )}
                          </button>
                          {getItem(value.cod_producto, cartProducts).cant}{" "}
                          <button
                            onClick={() => handleIncrement(value.cod_producto)}
                            className="ml-4"
                          >
                            {" "}
                            {getItem(value.cod_producto, cartProducts).cant <
                            value.stock
                              ? "+"
                              : ""}
                          </button>
                        </div>
                      ) : (
                        <button
                          onClick={() => addProductToCart(value)}
                          style={{
                            fontSize: "0.9rem",
                            backgroundColor: "#3f51b5",
                            flex: "1 1",
                            alignItems: "flex-end",
                          }}
                          type="button"
                          className="btn btn-block mt-0 btn-primary"
                          data-toggle="button"
                          aria-pressed="false"
                        >
                          Agregar
                        </button>
                      )}
                    </div>
                  </div>
                );
              })}
            </>
          );
        })}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  var products = state.cart.products;
  return {
    cartProducts: products,
  };
};
const mapDispatchToProps = {
  addProductCart,
  incrementProduct,
  decrementProduct,
};
export default connect(mapStateToProps, mapDispatchToProps)(ProductsStore);

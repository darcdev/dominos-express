const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN_REQUEST":
      return {
        ...state,
        user: action.payload,
      };
    case "LOGOUT_REQUEST":
      return {
        ...state,
        user: action.payload,
      };
    case "REGISTER_REQUEST":
      return {
        ...state,
        user: action.payload,
      };
    case "CART_VIEW_REQUEST":
      return state.cart;

    case "CART_EMPTY_REQUEST":
      window.localStorage.setItem("cart", JSON.stringify(action.payload));
      return {
        ...state,
        cart: {
          ...state.cart,
          products: action.payload,
        },
      };
    case "UPDATE_PRODUCT":
      window.localStorage.setItem("cart", JSON.stringify([...action.payload]));
      return {
        ...state,
        cart: {
          ...state.cart,
          products: [...action.payload],
        },
      };
    case "ADD_PRODUCT_CART":
      window.localStorage.setItem(
        "cart",
        JSON.stringify([...state.cart.products, action.payload])
      );
      return {
        ...state,
        cart: {
          ...state.cart,
          products: [...state.cart.products, action.payload],
        },
      };

    default:
      return state;
  }
};
export default reducer;
